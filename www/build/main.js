webpackJsonp([22],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdjustmentPlanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AdjustmentPlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdjustmentPlanPage = /** @class */ (function () {
    function AdjustmentPlanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AdjustmentPlanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdjustmentPlanPage');
    };
    AdjustmentPlanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-adjustment-plan',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/adjustment-plan/adjustment-plan.html"*/'<!--\n  Generated template for the AdjustmentPlanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>adjustment-plan</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/adjustment-plan/adjustment-plan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AdjustmentPlanPage);
    return AdjustmentPlanPage;
}());

//# sourceMappingURL=adjustment-plan.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AwaitingAuditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AwaitingAuditPage = /** @class */ (function () {
    function AwaitingAuditPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AwaitingAuditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AwaitingAuditPage');
    };
    AwaitingAuditPage.prototype.choiceGood = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    AwaitingAuditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-awaiting-audit',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/awaiting-audit/awaiting-audit.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <!-- <ion-navbar>\n    <ion-title>choice-item</ion-title>\n  </ion-navbar> -->\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="page-content">\n      <div class="header-img">\n        <img src="../../assets/imgs/awaiting-right.png" alt="">\n      </div>\n      <div>申请成功</div>\n      <div>等待管理员审核</div>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'choiceGood()\'>\n        进入首页\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/awaiting-audit/awaiting-audit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AwaitingAuditPage);
    return AwaitingAuditPage;
}());

//# sourceMappingURL=awaiting-audit.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComfirmPlanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_plan_create_plan__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ComfirmPlanPage = /** @class */ (function () {
    function ComfirmPlanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showModal = false;
    }
    ComfirmPlanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ComfirmPlanPage');
    };
    ComfirmPlanPage.prototype.pubFankui = function () {
        this.showModal = true;
    };
    ComfirmPlanPage.prototype.createPlan = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__create_plan_create_plan__["a" /* CreatePlanPage */]);
    };
    ComfirmPlanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-comfirm-plan',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/comfirm-plan/comfirm-plan.html"*/'<!--\n  Generated template for the ComfirmPlanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>完成计划</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="top-title">\n      请对您的本周计划做个评价：\n    </div>\n    <div class="content-title">3.1-3.5</div>\n    <div class="content-center">\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周一11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周二11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周三11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周四11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周五11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周六11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n      <div class="list">\n        <div class="list-item">\n          <div class="sub-title">\n            <div>\n              <checkbox></checkbox>\n            </div>\n            <div class="center-item">\n              饭后散步\n            </div>\n            <div>周日11：00</div>\n          </div>\n          <div class="evaluate-box">\n            <evaluate></evaluate>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'pubFankui()\'>\n        提交反馈\n      </div>\n    </div>\n\n    <div class="pos-modal" *ngIf=\'showModal\'>\n      <div class="pos-dialog">\n        <div class="pos-content">\n          <div class="pos-title">\n            <div class="pos-title-img">\n                <img src="../../assets/imgs/damuzhi.png" alt="">\n                <div class="pos-img">\n                    <img src="../../assets/imgs/san-ge-xin.png" alt="">\n                  </div>\n            </div>\n          </div>\n          <div class="pos-content-item">\n            <div class="pos-content-title">\n              你很棒棒哒\n            </div>\n            <div class="pos-content-center">\n              恭喜你已完成本周计划总结反馈，请继续努力保持做计划！\n            </div>\n          </div>\n          <div class="pos-footer">\n            <div class="btn">\n              <button class="button" (click)=\'tiaozheng()\'>调整计划</button>\n            </div>\n            <div class="btn">\n              <button class="button" (click)=\'createPlan()\'>新建计划</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/comfirm-plan/comfirm-plan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ComfirmPlanPage);
    return ComfirmPlanPage;
}());

//# sourceMappingURL=comfirm-plan.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreatePlanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreatePlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreatePlanPage = /** @class */ (function () {
    function CreatePlanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showPlan = false;
        this.juwei = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
        this.hourMinute = [
            {
                name: 'hours',
                options: [
                    { text: '8：00——10：00', value: '0' },
                    { text: '16：00——18：00', value: '1' }
                ]
            }
        ];
    }
    CreatePlanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecordBloodPressurePage');
    };
    CreatePlanPage.prototype.publicPlan = function () {
        this.showPlan = true;
    };
    CreatePlanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-plan',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/create-plan/create-plan.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>记录血压</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-content">\n      <ul>\n        <li>\n          <!-- <div class="box">\n                <span>收缩压</span>\n              </div> -->\n          <div class="box first-box">\n            <input type="text" placeholder="计划内容（如散步、控制饮食）">\n          </div>\n        </li>\n        <li>\n          <!-- <div class="box">\n                    <span>收缩压</span>\n                  </div> -->\n          <div class="box first-box">\n            <input type="text" placeholder="做多少（如散步10分钟）">\n          </div>\n        </li>\n        <li>\n          <!-- <div class="box">\n                        <span>收缩压</span>\n                      </div> -->\n          <div class="box first-box">\n            <input type="text" placeholder="计划何时执行">\n          </div>\n        </li>\n        <li>\n          <!-- <div class="box">\n            <span>计划内容（如散步、控制饮食）</span>\n          </div> -->\n          <div class="box">\n            <ion-item>\n              <ion-multi-picker item-content [multiPickerColumns]="hourMinute" placeholder="一周进行次数" doneText="确定"\n                cancelText="取消">\n              </ion-multi-picker>\n            </ion-item>\n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n        <li>\n          <!-- <div class="box">\n              <span>计划内容（如散步、控制饮食）</span>\n            </div> -->\n          <div class="box">\n            <ion-item>\n              <ion-multi-picker item-content [multiPickerColumns]="hourMinute" placeholder="对完成计划的自信心" doneText="确定"\n                cancelText="取消">\n              </ion-multi-picker>\n            </ion-item>\n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'publicPlan()\'>\n        提交计划\n      </div>\n    </div>\n\n    <div class="pos-modal" *ngIf=\'showPlan\'>\n      <div class="pos-dialog">\n        <div class="pos-content">\n          <div class="pos-title">\n            <img src="../../assets/imgs/notice-line.png" alt="">\n            <div>计划确认</div>\n            <img src="../../assets/imgs/notice-line.png" alt="">\n          </div>\n          <div class="pos-content-item">\n            <div class="pos-content-item-title">\n              您本周计划内容为：\n            </div>\n            <div class="pos-content-content">\n              <div class="item">\n                <img src="../../assets/imgs/plan-react.png" alt="">\n                <div>饭后散步10分钟</div>\n              </div>\n              <div class="item">\n                <img src="../../assets/imgs/plan-react.png" alt="">\n                <div>饭后散步10分钟</div>\n              </div>\n            </div>\n          </div>\n          <div class="pos-footer">\n            <div class="btn">\n              <button class="button" (click)=\'tiaozheng()\'>容我想想</button>\n            </div>\n            <div class="btn">\n              <button class="button" (click)=\'createPlan()\'>我确定</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/create-plan/create-plan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], CreatePlanPage);
    return CreatePlanPage;
}());

//# sourceMappingURL=create-plan.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoArticlePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the VideoArticlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoArticlePage = /** @class */ (function () {
    function VideoArticlePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    VideoArticlePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VideoArticlePage');
    };
    VideoArticlePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-video-article',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/video-article/video-article.html"*/'<!--\n  Generated template for the VideoArticlePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>文章详情</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="top-video">\n      <video src="" controls></video>\n    </div>\n    <div class="content">\n\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/video-article/video-article.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], VideoArticlePage);
    return VideoArticlePage;
}());

//# sourceMappingURL=video-article.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_info_chat_info__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChatPage = /** @class */ (function () {
    function ChatPage(navParams, navCtrl) {
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.toUser = {
            _id: '534b8e5aaa5e7afc1b23e69b',
            pic: '../../assets/imgs/me.png',
            username: 'Venkman',
        };
        this.user = {
            _id: '534b8fb2aa5e7afc1b23e69c',
            pic: '../../assets/imgs/me.png',
            username: 'Marty',
        };
        this.showNewsNum = true;
        this.doneLoading = false;
        this.messages = [
            {
                _id: 1,
                date: new Date(),
                userId: this.user._id,
                username: this.user.username,
                pic: this.user.pic,
                text: 'OH CRAP!!'
            },
            {
                _id: 2,
                date: new Date(),
                userId: this.toUser._id,
                username: this.toUser.username,
                pic: this.toUser.pic,
                text: 'what??'
            },
            {
                _id: 3,
                date: new Date(),
                userId: this.toUser._id,
                username: this.toUser.username,
                pic: this.toUser.pic,
                text: 'Pretty long message with lots of content'
            },
            {
                _id: 4,
                date: new Date(),
                userId: this.user._id,
                username: this.user.username,
                pic: this.user.pic,
                text: 'Pretty long message with even way more of lots and lots of content'
            },
            {
                _id: 5,
                date: new Date(),
                userId: this.user._id,
                username: this.user.username,
                pic: this.user.pic,
                text: '哪尼??'
            },
            {
                _id: 6,
                date: new Date(),
                userId: this.toUser._id,
                username: this.toUser.username,
                pic: this.toUser.pic,
                text: 'yes!'
            }
        ];
        this.audio = false;
    }
    ChatPage.prototype.ionViewDidLoad = function () {
        var modelData = '用户名：' + this.navParams.get('chatId');
        console.log(modelData);
    };
    // 发送消息
    ChatPage.prototype.send = function (message) {
        var _this = this;
        if (message && message !== '') {
            // this.messageService.sendMessage(chatId, message);
            var messageData = {
                toId: this.toUser._id,
                _id: 6,
                date: new Date(),
                userId: this.user._id,
                username: this.toUser.username,
                pic: this.toUser.pic,
                text: message
            };
            this.messages.push(messageData);
            this.scrollToBottom();
            setTimeout(function () {
                var replyData = {
                    toId: _this.toUser._id,
                    _id: 6,
                    date: new Date(),
                    userId: _this.toUser._id,
                    username: _this.toUser.username,
                    pic: _this.toUser.pic,
                    text: 'Just a quick reply'
                };
                _this.messages.push(replyData);
                _this.scrollToBottom();
            }, 1000);
        }
        this.chatBox = '';
    };
    ChatPage.prototype.scrollToBottom = function () {
        var _this = this;
        setTimeout(function () {
            _this.content.scrollToBottom();
        }, 100);
    };
    ChatPage.prototype.viewProfile = function (message) {
        console.log(message);
    };
    ChatPage.prototype.doRefresh = function (refresher) {
        console.log("下拉刷新");
        setTimeout(function () {
            console.log('加载完成后，关闭刷新');
            refresher.complete();
            //toast提示
            // this.showInfo("加载成功");
        }, 2000);
    };
    ChatPage.prototype.changeAudio = function () {
        console.log(121);
        if (this.audio) {
            this.audio = false;
        }
        else {
            this.audio = true;
        }
    };
    ChatPage.prototype.btnTouchstart = function () {
        console.log('start');
    };
    ChatPage.prototype.btnTouchend = function () {
        console.log('end');
    };
    ChatPage.prototype.allNews = function () {
        this.showNewsNum = false;
    };
    ChatPage.prototype.chatGroup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_info_chat_info__["a" /* ChatInfoPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"])
    ], ChatPage.prototype, "content", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat/chat.html"*/'<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>群组聊天</ion-title>\n    <button ion-button clear icon-right (click)=\'chatGroup()\' style="padding: 0; position: absolute; right: 6px; top: 6px;height: 22px;background: #58B5FC">\n      <img class="back-btn" src="../../../assets/imgs/chat-list.png" />\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="下拉刷新" refreshingSpinner="circles" refreshingText="刷新...">\n    </ion-refresher-content>\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="normal-page">\n    <div class="pos-new-num" *ngIf="showNewsNum" (click)=\'allNews()\'>\n      104条新消息\n      <img src="../../assets/imgs/to-top-icon.png" alt="">\n    </div>\n    <div *ngFor="let message of messages" class="message-wrapper" on-hold="onMessageHold($event, $index, message)">\n      <!-- 判断消息是发送 -->\n      <div *ngIf="user._id !== message.userId" class=\'message\'>\n        <div class="chat-user">\n          <div class="header-img margin-right5">\n            <img (click)="viewProfile(message)" class="profile-pic left" [src]="toUser.pic" />\n          </div>\n          <div class="chat-bubble">\n            <div class="chat-username">{{user.username}}</div>\n            <div class="chat-message" [innerHTML]="message.text" autolinker> </div>\n            <div class="message-detail">\n              <span>{{message.date | moment:"ago" | lowercase}}</span>\n            </div>\n          </div>\n        </div>\n\n        <!--  wave-->\n\n      </div>\n      <!-- 判断消息是发送 -->\n      <div *ngIf="user._id == message.userId" class=\'message\'>\n        <div class="chat-user flex-end">\n          <div class="chat-bubble text-right">\n            <div class="chat-username">{{user.username}}</div>\n            <div class="chat-message" [innerHTML]="message.text" autolinker> </div>\n            <div class="message-detail">\n              <span>{{message.date | moment:"ago" | lowercase}}</span>\n            </div>\n          </div>\n          <div class="header-img margin-left5">\n            <img (click)="viewProfile(message)" class="profile-pic left" [src]="toUser.pic" />\n          </div>\n        </div>\n        <div class="cf"></div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>\n<!-- 底部固定的输入框 -->\n<ion-footer>\n  <ion-item *ngIf=\'!audio\'>\n    <span (click)=\'changeAudio()\' class=\'footer-item-start\' item-start>\n      <img src=\'../../assets/imgs/yuyin.png\'>\n    </span>\n    <ion-input class=\'footer-item-input\' [(ngModel)]="chatBox" placeholder="Send {{toUser.username}} a message..."></ion-input>\n    <span class=\'footer-item-end\' (click)="send(chatBox)" item-end>\n      <!-- <ion-icon class="footer-btn" name="send"></ion-icon> -->\n      发送\n    </span>\n  </ion-item>\n\n  <ion-item *ngIf=\'audio\'>\n    <span (click)=\'changeAudio()\' class=\'footer-item-start\' item-start>\n      <img src=\'../../assets/imgs/jianpan.png\'>\n    </span>\n    <button class="footer-item-btn" (touchstart)=\'btnTouchstart()\' (touchend)=\'btnTouchend()\'>\n      按住说话\n    </button>\n    <span class=\'footer-item-end\' (click)="send(chatBox)" item-end>\n      <!-- <ion-icon class="footer-btn" name="send"></ion-icon> -->\n      发送\n    </span>\n  </ion-item>\n</ion-footer>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_group_chat_group__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_record_chat_record__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatInfoPage = /** @class */ (function () {
    function ChatInfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.isBegin = false;
    }
    ChatInfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatInfoPage');
    };
    ChatInfoPage.prototype.choiceBegin = function () {
        this.isBegin = true;
    };
    ChatInfoPage.prototype.choiceClose = function () {
        this.isBegin = false;
    };
    ChatInfoPage.prototype.choiceQuit = function () {
    };
    ChatInfoPage.prototype.toGroup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_group_chat_group__["a" /* ChatGroupPage */]);
    };
    ChatInfoPage.prototype.toRecord = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_record_chat_record__["a" /* ChatRecordPage */]);
    };
    ChatInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat-info',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-info/chat-info.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>群组信息</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-content">\n      <ul>\n        <li>\n          <div class="box">\n            <span>群组名称</span>\n          </div>\n          <div class="box">\n            <span>徐汇四和花园居委糖尿病—A组</span>\n          </div>\n        </li>\n        <li (click)=\'toGroup()\'>\n          <div class="box">\n            <span>群组成员</span>\n          </div>\n          <div class="box">\n            <ion-icon name="arrow-forward"></ion-icon>\n          </div>\n        </li>\n        <li (click)=\'toRecord()\'>\n          <div class="box">\n            <span>聊天记录</span>\n          </div>\n          <div class="box"> \n            <ion-icon name="arrow-forward"></ion-icon>\n          </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>消息免打扰</span>\n          </div>\n          <div class="box">\n            <div class="switch-box">\n              <div [ngClass]="{\'switch-item1\': true, \'bgacolor\': isBegin ? true : false}" (click)=\'choiceBegin()\'>开</div>\n              <div [ngClass]="{\'switch-item2\': true, \'bgacolor\': isBegin ? false : true}" (click)=\'choiceClose()\'>关</div>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'choiceQuit()\'>\n        退出当前小组\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-info/chat-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChatInfoPage);
    return ChatInfoPage;
}());

//# sourceMappingURL=chat-info.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatRecordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ChatRecordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatRecordPage = /** @class */ (function () {
    function ChatRecordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChatRecordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatRecordPage');
    };
    ChatRecordPage.prototype.ngOnInit = function () {
        this.storgeData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？",
            }, {
                id: 2,
                text: "孕期运动",
            }, {
                id: 3,
                text: "孕早期",
            }, {
                id: 4,
                text: "妊娠期",
            }
        ];
        this.allData = [
            {
                id: 1,
                content: '石门二路街道善昌居委会',
            }, {
                id: 2,
                content: '消息在群公告内，你可以去查看消息在群公告内，你可以去查看',
            }, {
                id: 3,
                content: '石门二路街道善昌居委会(本部)',
            }
        ];
        console.log('ionViewDidLoad KnowledgeBasePage');
    };
    ChatRecordPage.prototype.toPage = function (id) {
        // this.navCtrl.push(KnowledgeDetailPage);
    };
    ChatRecordPage.prototype.inputFocus = function () {
        if (this.keyword != '') {
            this.searching = true;
        }
    };
    ChatRecordPage.prototype.clearAll = function () {
        this.storgeData = [];
    };
    ChatRecordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat-record',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-record/chat-record.html"*/'<ion-header>\n\n    <ion-navbar>\n      <ion-title>search</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content>\n    <div class="normal-page">\n      <div class="top-bar">\n          <input type="text" placeholder="请输入关键字" (focus)=\'inputFocus()\' [(ngModel)]="keyword"><ion-icon class="search" name="search"></ion-icon>\n      </div>\n      <ul class="flex-ul" *ngIf=\'searching\'>\n          <li *ngFor="let i of allData" (click)=\'toPage(i.id)\'>\n              <div class="header-img">\n                <img src="../../assets/imgs/man.png" alt="">\n              </div>\n              <div class="center-content">\n                <div>\n                  陈逸飞 \n                </div>\n                <div>\n                    <span [innerHTML]=\'i.content | wordPlace:keyword\'></span>\n                </div>\n              </div>\n              <div class="end-date">\n                1月24日\n              </div>\n              <!-- <div class="text-item"><span [innerHTML]=\'i.content | wordPlace:keyword\'></span></div> -->\n          </li>\n      </ul>\n  </div>\n  </ion-content>\n  '/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-record/chat-record.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChatRecordPage);
    return ChatRecordPage;
}());

//# sourceMappingURL=chat-record.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CaseNoticePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CaseNoticePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CaseNoticePage = /** @class */ (function () {
    function CaseNoticePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CaseNoticePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CaseNoticePage');
    };
    CaseNoticePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-case-notice',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/case-notice/case-notice.html"*/'<!--\n  Generated template for the CaseNoticePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>case-notice</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  \n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/case-notice/case-notice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], CaseNoticePage);
    return CaseNoticePage;
}());

//# sourceMappingURL=case-notice.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeNoticePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notice_detail_notice_detail__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_notice_create_notice__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeNoticePage = /** @class */ (function () {
    function HomeNoticePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HomeNoticePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatGroupPage');
        this.listData = [
            {
                notice: '石门二路街道善昌居委会社区活动；',
                address: '静安区石门二路街道善昌居委会',
                time: '2月13日 14：30',
                content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
                looks: 10,
                userTitle: '组长张清晨',
                dateTime: '2019年1月20日'
            }, {
                notice: '石门二路街道善昌居委会社区活动；',
                address: '静安区石门二路街道善昌居委会',
                time: '2月13日 14：30',
                content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
                looks: 10,
                userTitle: '组长张清晨',
                dateTime: '2019年1月20日'
            }, {
                notice: '石门二路街道善昌居委会社区活动；',
                address: '静安区石门二路街道善昌居委会',
                time: '2月13日 14：30',
                content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
                looks: 10,
                userTitle: '组长张清晨',
                dateTime: '2019年1月20日'
            }
        ];
    };
    HomeNoticePage.prototype.createNotice = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__create_notice_create_notice__["a" /* CreateNoticePage */]);
    };
    HomeNoticePage.prototype.toNoticeDetail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__notice_detail_notice_detail__["a" /* NoticeDetailPage */]);
    };
    HomeNoticePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home-notice',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/home-notice/home-notice.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>公告通知</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="list-content">\n      <div class="list-title">\n        <img src="../../assets/imgs/notice-line.png" alt="">\n        <div>最新公告</div>\n        <img src="../../assets/imgs/notice-line.png" alt="">\n      </div>\n      <ul>\n        <li class="first-li" *ngFor=\'let item of listData, let i = index\' [ngClass]="{\'first-li\': i==0?true:false}" (click)=\'toNoticeDetail()\'>\n          <div class="notice-item">\n            <div class="title">\n              通知:\n            </div>\n            <div class="content">{{item.notice}}</div>\n          </div>\n          <div class="notice-item" *ngIf=\'i == 0\'>\n            <div class="title">\n              地点:\n            </div>\n            <div class="content">{{item.address}}</div>\n          </div>\n          <div class="notice-item" *ngIf=\'i == 0\'>\n            <div class="title">\n              时间:\n            </div>\n            <div class="content">{{item.time}}</div>\n          </div>\n          <div class="notice-item2" *ngIf=\'i == 0\'>\n            <div class="title">公告内容:</div>\n            <div class="content">{{item.content}}</div>\n          </div>\n          <div class="notice-item3">\n            <div class="left-content">\n              <img src="../../assets/imgs/eye.png" alt="">\n              <div>{{item.looks}}</div>\n            </div>\n            <div class="right-content">\n              <div>{{item.userTitle}}</div>\n              <div>|</div>\n              <div>{{item.dateTime}}</div>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'createNotice()\'>\n        新建公告\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/home-notice/home-notice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HomeNoticePage);
    return HomeNoticePage;
}());

//# sourceMappingURL=home-notice.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NoticeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NoticeDetailPage = /** @class */ (function () {
    function NoticeDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NoticeDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NoticeDetailPage');
    };
    NoticeDetailPage.prototype.createNotice = function () {
    };
    NoticeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notice-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/notice-detail/notice-detail.html"*/'<!--\n  Generated template for the NoticeDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>notice-detail</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <ul>\n      <li class="first-li">\n        <div class="notice-item">\n          <div class="title">\n            通知:\n          </div>\n          <div class="content">石门二路街道善昌居委会社区活动；</div>\n        </div>\n        <div class="notice-item3">\n          <div class="left-content">\n            <div>组长张清晨</div>\n            <div>|</div>\n            <div>2019年1月20日</div>\n          </div>\n\n          <div class="right-content">\n            <img src="../../assets/imgs/eye.png" alt="">\n            <div>10</div>\n          </div>\n        </div>\n        <div class="notice-item">\n          <div class="title">\n            地点:\n          </div>\n          <div class="content">静安区石门二路街道善昌居委会</div>\n        </div>\n        <div class="notice-item">\n          <div class="title">\n            时间:\n          </div>\n          <div class="content">2月13日 14：30</div>\n        </div>\n        <div class="notice-item2">\n          <div class="title">公告内容:</div>\n          <div class="content">的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者</div>\n        </div>\n      </li>\n    </ul>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'createNotice()\'>\n        新建公告\n      </div>\n      <div class="choice-btn" (click)=\'createNotice()\'>\n        删除公告\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/notice-detail/notice-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], NoticeDetailPage);
    return NoticeDetailPage;
}());

//# sourceMappingURL=notice-detail.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateNoticePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreateNoticePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateNoticePage = /** @class */ (function () {
    function CreateNoticePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.juwei = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
        this.hourMinute = [
            {
                name: 'hours',
                options: [
                    { text: '00', value: '00' },
                    { text: '01', value: '01' },
                    { text: '02', value: '02' },
                    { text: '03', value: '03' },
                    { text: '04', value: '04' },
                    { text: '05', value: '05' },
                    { text: '06', value: '06' },
                    { text: '07', value: '07' },
                    { text: '08', value: '08' },
                    { text: '09', value: '09' },
                    { text: '10', value: '10' },
                    { text: '11', value: '11' },
                    { text: '12', value: '12' },
                    { text: '13', value: '13' },
                    { text: '14', value: '14' },
                    { text: '15', value: '15' },
                    { text: '16', value: '16' },
                    { text: '17', value: '17' },
                    { text: '18', value: '18' },
                    { text: '19', value: '19' },
                    { text: '20', value: '20' },
                    { text: '21', value: '21' },
                    { text: '22', value: '22' },
                    { text: '23', value: '23' },
                ]
            }, {
                name: 'mao',
                options: [
                    { text: ':', value: ':' },
                ]
            }, {
                name: 'minutes',
                options: [
                    { text: '00', value: '00' },
                    { text: '01', value: '01' },
                    { text: '02', value: '02' },
                    { text: '03', value: '03' },
                    { text: '04', value: '04' },
                    { text: '05', value: '05' },
                    { text: '06', value: '06' },
                    { text: '07', value: '07' },
                    { text: '08', value: '08' },
                    { text: '09', value: '09' },
                    { text: '10', value: '10' },
                    { text: '11', value: '11' },
                    { text: '12', value: '12' },
                    { text: '13', value: '13' },
                    { text: '14', value: '14' },
                    { text: '15', value: '15' },
                    { text: '16', value: '16' },
                    { text: '17', value: '17' },
                    { text: '18', value: '18' },
                    { text: '19', value: '19' },
                    { text: '20', value: '20' },
                    { text: '21', value: '21' },
                    { text: '22', value: '22' },
                    { text: '23', value: '23' },
                    { text: '24', value: '24' },
                    { text: '25', value: '25' },
                    { text: '26', value: '26' },
                    { text: '27', value: '27' },
                    { text: '28', value: '28' },
                    { text: '29', value: '29' },
                    { text: '30', value: '30' },
                    { text: '31', value: '31' },
                    { text: '32', value: '32' },
                    { text: '33', value: '33' },
                    { text: '34', value: '34' },
                    { text: '35', value: '35' },
                    { text: '36', value: '36' },
                    { text: '37', value: '37' },
                    { text: '38', value: '38' },
                    { text: '39', value: '39' },
                    { text: '40', value: '40' },
                    { text: '41', value: '41' },
                    { text: '42', value: '42' },
                    { text: '43', value: '43' },
                    { text: '44', value: '44' },
                    { text: '45', value: '45' },
                    { text: '46', value: '46' },
                    { text: '47', value: '47' },
                    { text: '48', value: '48' },
                    { text: '49', value: '49' },
                    { text: '50', value: '50' },
                    { text: '51', value: '51' },
                    { text: '52', value: '52' },
                    { text: '53', value: '53' },
                    { text: '54', value: '54' },
                    { text: '55', value: '55' },
                    { text: '56', value: '56' },
                    { text: '57', value: '57' },
                    { text: '58', value: '58' },
                    { text: '59', value: '59' },
                ]
            }
        ];
    }
    CreateNoticePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateNoticePage');
    };
    CreateNoticePage.prototype.publicNotice = function () {
    };
    CreateNoticePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-notice',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/create-notice/create-notice.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>新建公告</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-content">\n      <ul>\n        <li>\n          <div class="box">\n            <span>公告标题</span>\n          </div>\n          <div class="box first-box">\n            <input type="text">\n          </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>活动日期</span>\n          </div>\n          <div class="box">\n            <ion-item>\n              <ion-datetime class=\'text-input ion-datetime font-color3\' displayFormat="YYYY-MM-DD" cancelText="取消" doneText="确定" showReset=\'选择日期\' resetText=\'选择日期\' placeholder="2018-10-01"\n                    [(ngModel)]="myDate"></ion-datetime>\n            </ion-item>\n            \n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>活动时间</span>\n          </div>\n          <div class="box">\n            <ion-item>\n              <ion-multi-picker item-content [multiPickerColumns]="hourMinute" doneText="确定" cancelText="取消"></ion-multi-picker>\n            </ion-item>\n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>活动地点</span>\n          </div>\n          <div class="box">\n            <ion-item>\n              <ion-multi-picker item-content [multiPickerColumns]="juwei" placeholder=\'居委\' doneText="确定" cancelText="取消"></ion-multi-picker>\n            </ion-item>\n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n\n        <li class="end-item">\n          <div class="">\n            <span>活动地点</span>\n          </div>\n          <div class="box-end">\n            <textarea name="" id="" rows="5"></textarea>\n          </div>\n        </li>\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'publicNotice()\'>\n        发布公告\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/create-notice/create-notice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], CreateNoticePage);
    return CreateNoticePage;
}());

//# sourceMappingURL=create-notice.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecordBloodPressurePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__blood_pressure_data_blood_pressure_data__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RecordBloodPressurePage = /** @class */ (function () {
    function RecordBloodPressurePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.juwei = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
        this.hourMinute = [
            {
                name: 'hours',
                options: [
                    { text: '8：00——10：00', value: '0' },
                    { text: '16：00——18：00', value: '1' }
                ]
            }
        ];
    }
    RecordBloodPressurePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecordBloodPressurePage');
    };
    RecordBloodPressurePage.prototype.publicNotice = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__blood_pressure_data_blood_pressure_data__["a" /* BloodPressureDataPage */]);
    };
    RecordBloodPressurePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-record-blood-pressure',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/record-blood-pressure/record-blood-pressure.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>记录血压</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-content">\n      <ul>\n        <li>\n          <div class="box">\n            <span>请选择时间</span>\n          </div>\n          <div class="box">\n            <ion-item>\n              <ion-multi-picker item-content [multiPickerColumns]="hourMinute" doneText="确定" cancelText="取消">\n              </ion-multi-picker>\n            </ion-item>\n            <!-- <ion-icon name="arrow-forward"></ion-icon> -->\n          </div>\n        </li>\n        <li>\n        <div class="box">\n          <span>收缩压</span>\n        </div>\n        <div class="box first-box">\n          <input type="text">\n        </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>舒张压</span>\n          </div>\n          <div class="box first-box">\n            <input type="text">\n          </div>\n        </li>\n        <li>\n          <div class="box">\n            <span>脉搏</span>\n          </div>\n          <div class="box first-box">\n            <input type="text">\n          </div>\n        </li>\n\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'publicNotice()\'>\n        完成\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/record-blood-pressure/record-blood-pressure.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RecordBloodPressurePage);
    return RecordBloodPressurePage;
}());

//# sourceMappingURL=record-blood-pressure.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BloodPressureDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BloodPressureDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BloodPressureDataPage = /** @class */ (function () {
    function BloodPressureDataPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pushModal = false;
    }
    BloodPressureDataPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BloodPressureDataPage');
    };
    BloodPressureDataPage.prototype.checkBloodPressure = function () {
    };
    BloodPressureDataPage.prototype.checkConform = function () {
    };
    BloodPressureDataPage.prototype.closeModal = function () {
        this.pushModal = false;
    };
    BloodPressureDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-blood-pressure-data',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/blood-pressure-data/blood-pressure-data.html"*/'<!--\n  Generated template for the BloodPressureDataPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>血压数据</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="flex-box">\n      <div class="left-box">\n        <div>日期：2019年1月2日</div>\n        <div>时间：16：00——18：00</div>\n        <div>收缩压：110mmHg</div>\n        <div>舒张压：80mmHg</div>\n        <div>脉搏：75次/分</div>\n      </div>\n      <div class="right-box">\n        <span>正常</span>\n      </div>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'checkBloodPressure()\'>\n        查看血压曲线\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/blood-pressure-data/blood-pressure-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], BloodPressureDataPage);
    return BloodPressureDataPage;
}());

//# sourceMappingURL=blood-pressure-data.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionnairePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the QuestionnairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuestionnairePage = /** @class */ (function () {
    function QuestionnairePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 220,
            'canvasHeight': 120
        };
        this.showModal = true;
    }
    QuestionnairePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QuestionnairePage');
    };
    QuestionnairePage.prototype.ngAfterViewInit = function () {
        this.signaturePad.clear();
        this.canvasResize();
    };
    // 清除模板
    QuestionnairePage.prototype.drawClear = function () {
        this.signaturePad.clear();
    };
    QuestionnairePage.prototype.canvasResize = function () {
        var canvas = document.querySelector('canvas');
        this.signaturePad.set('minWidth', 2);
        this.signaturePad.set('canvasWidth', canvas.offsetWidth);
        this.signaturePad.set('canvasHeight', canvas.offsetHeight);
    };
    // 完成生成图片
    QuestionnairePage.prototype.drawComplete = function (sign) {
        this.signatureImage = this.signaturePad.toDataURL();
        console.log(this.signatureImage);
    };
    QuestionnairePage.prototype.knows = function () {
        this.showModal = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__["SignaturePad"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__["SignaturePad"])
    ], QuestionnairePage.prototype, "signaturePad", void 0);
    QuestionnairePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-questionnaire',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/questionnaire/questionnaire.html"*/'<!--\n  Generated template for the QuestionnairePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color="primary">\n        <ion-title>\n            问卷调查\n        </ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-top">\n      <img src="../../assets/imgs/circle.png" alt="">\n      <img src="../../assets/imgs/notice-line.png" alt="">\n      <div class="text">\n        个人信息\n      </div>\n      <img src="../../assets/imgs/notice-line.png" alt="">\n      <img src="../../assets/imgs/circle.png" alt="">\n    </div>\n    <div class="page-center">\n      <div class="title">请填写个人信息</div>\n      <div class="inp">\n        <input type="text" placeholder="姓名">\n      </div>\n      <div class="inp">\n        <input type="text" placeholder="联系方式">\n      </div>\n      <div class="inp">\n        <input type="text" placeholder="家庭住址">\n      </div>\n      <div class="inp">\n        <input type="text" placeholder="年龄">\n      </div>\n      <div class="inp">\n        <input type="text" placeholder="学历">\n      </div>\n      <div class="inp">\n        <input type="text" placeholder="配偶">\n      </div>\n    </div>\n    <div class="tips-page">\n      1/6\n    </div>\n    <div class="page-footer">\n      <button class="button">下一步</button>\n    </div>\n\n    <div class="pos-modal" *ngIf=\'showModal\'>\n      <div class="pos-dialog">\n        <div class="pos-content">\n          <div class="title">\n              问卷开始前，请您仔细阅读以下知情同意书\n          </div>\n          <div class="content">\n              知情同意书研究项目名称因为您具备XXX研究的入组条件，所以我们邀请您参加这项研究。请您仔细阅读本知情同意书并慎重做出是否参加研究的决定。研究医生或者研究人员非常愿意与您讨论并解释知情同意书中您不明白的相关内容。在您做出是否参与此项研究的决定之前，您可以和的家人及朋友进行充分讨论。若您正在参加其他研究，请务必告诉您的研究医生或者研究人员。 XXX作为组长单位的主要研究者负责开展这项由XXX资助的研究。\n          </div>\n          <div class="footer">\n            <div class="right-sign">\n              <span>签名：</span>\n              <signature-pad [options]="signaturePadOptions" id="signatureCanvas"></signature-pad>\n              <span (click)=\'drawClear()\'>清除</span>\n            </div>\n            <!-- <div>\n              <button (click)=\'drawComplete()\'>显示图片</button>\n              <img [src]="signatureImage" *ngIf="signatureImage" />\n            </div> -->\n            <div class="btn">\n              <button class="button" (click)=\'knows()\'>我知道了</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/questionnaire/questionnaire.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], QuestionnairePage);
    return QuestionnairePage;
}());

//# sourceMappingURL=questionnaire.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChoiceItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__awaiting_audit_awaiting_audit__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChoiceItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChoiceItemPage = /** @class */ (function () {
    function ChoiceItemPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChoiceItemPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChoiceItemPage');
        this.listData = [
            {
                show: false,
                title1: '徐汇石门二路街道善昌居委—糖尿病A组',
                title2: '每周三14：30'
            }, {
                show: false,
                title1: '徐汇石门二路街道善昌居委—糖尿病A组',
                title2: '每周三14：30'
            }, {
                show: false,
                title1: '徐汇石门二路街道善昌居委—糖尿病A组',
                title2: '每周三14：30'
            }
        ];
    };
    ChoiceItemPage.prototype.search = function () {
        // this.navCtrl.push(SearchPage);
    };
    ChoiceItemPage.prototype.choiceCli = function (index) {
        for (var i = 0; i < this.listData.length; i++) {
            this.listData[i].show = false;
        }
        this.listData[index].show = true;
    };
    ChoiceItemPage.prototype.neighborhoodCommittee = function () {
    };
    ChoiceItemPage.prototype.choiceGood = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__awaiting_audit_awaiting_audit__["a" /* AwaitingAuditPage */]);
    };
    ChoiceItemPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-choice-item',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/choice-item/choice-item.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>choice-item</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="page-title">\n      请在选项中选择一个小组\n    </div>\n    <div class="page-content">\n      <ul>\n        <li *ngFor=\'let item of listData, let i = index\' [ngClass]=\'{"choice": item.show}\' (click)=\'choiceCli(i)\'>\n          <div class=\'title1\'>{{item.title1}}</div>\n          <div class=\'title2\'>活动时间：{{item.title2}}</div>\n          <div class="pos-right" *ngIf=\'item.show\'>\n            <img src="../../assets/imgs/right.png" alt="">\n          </div>\n        </li>\n      </ul>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-area">\n          选择自己合适的小组\n      </div>\n      <div class="choice-btn" (click)=\'choiceGood()\'>\n        完成\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/choice-item/choice-item.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChoiceItemPage);
    return ChoiceItemPage;
}());

//# sourceMappingURL=choice-item.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChoicePositionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_search__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__choice_item_choice_item__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChoicePositionPage = /** @class */ (function () {
    function ChoicePositionPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.areas = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
        this.streets = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
        this.juwei = [
            {
                name: 'col1',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' }
                ]
            }
        ];
    }
    ChoicePositionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChoicePositionPage');
    };
    ChoicePositionPage.prototype.search = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__search_search__["a" /* SearchPage */]);
    };
    ChoicePositionPage.prototype.choiceGood = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__choice_item_choice_item__["a" /* ChoiceItemPage */]);
    };
    ChoicePositionPage.prototype.areaCli = function () {
    };
    ChoicePositionPage.prototype.streetCli = function () {
    };
    ChoicePositionPage.prototype.neighborhoodCommittee = function () {
    };
    ChoicePositionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-choice-position',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/choice-position/choice-position.html"*/'<!--\n  Generated template for the ChoicePositionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>choice-position</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="page-title">\n      请填写您的个人信息\n    </div>\n    <div class="page-content">\n      <div class="inp-text">\n        <ion-item>\n          <ion-multi-picker item-content [multiPickerColumns]="areas" placeholder=\'地区\' doneText="确定" cancelText="取消"></ion-multi-picker>\n        </ion-item>\n      </div>\n      <div class="inp-text">\n        <ion-item>\n          <ion-multi-picker item-content [multiPickerColumns]="streets" placeholder=\'街道\' doneText="确定" cancelText="取消"></ion-multi-picker>\n        </ion-item>\n      </div>\n      <div class="inp-text">\n        <ion-item>\n          <ion-multi-picker item-content [multiPickerColumns]="juwei" placeholder=\'居委\' doneText="确定" cancelText="取消"></ion-multi-picker>\n        </ion-item>\n        <!-- <input type="text" placeholder="居委" > -->\n        <div class=\'pos-search\' (click)=\'search()\'>\n          <img src="../../assets/imgs/search.png" alt="">\n        </div>\n      </div>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-area">\n        城市：上海\n      </div>\n      <div class="choice-btn" (click)=\'choiceGood()\'>\n        下一步\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/choice-position/choice-position.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChoicePositionPage);
    return ChoicePositionPage;
}());

//# sourceMappingURL=choice-position.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage.prototype.ngOnInit = function () {
        this.storgeData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？",
            }, {
                id: 2,
                text: "孕期运动",
            }, {
                id: 3,
                text: "孕早期",
            }, {
                id: 4,
                text: "妊娠期",
            }
        ];
        this.allData = [
            {
                id: 1,
                content: '石门二路街道善昌居委会',
            }, {
                id: 2,
                content: '石门二路街道善昌居委会',
            }, {
                id: 3,
                content: '石门二路街道善昌居委会(本部)',
            }
        ];
        console.log('ionViewDidLoad KnowledgeBasePage');
    };
    SearchPage.prototype.toPage = function (id) {
        // this.navCtrl.push(KnowledgeDetailPage);
    };
    SearchPage.prototype.inputFocus = function () {
        if (this.keyword != '') {
            this.searching = true;
        }
    };
    SearchPage.prototype.clearAll = function () {
        this.storgeData = [];
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/search/search.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>search</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="top-bar">\n        <input type="text" placeholder="请输入关键字" (focus)=\'inputFocus()\' [(ngModel)]="keyword"><ion-icon class="search" name="search"></ion-icon>\n    </div>\n    <!-- <div class=\'search-text-box\' *ngIf=\'!searching\'>\n        <div class="box-title">\n            <span>搜索记录：</span>\n            <span (click)=\'clearAll()\'>\n                <img src="../../../assets/imgs/delete.png" alt="">\n            </span>\n        </div>\n        <div class="flex-box">\n            <div *ngFor="let i of storgeData">\n                <div class="for-list">\n                    {{i.text}}\n                </div>\n            </div>\n        </div>\n        <div class="box-title">大家都在搜：</div>\n        <div class="flex-box">\n            <div *ngFor="let i of storgeData">\n                <div class="for-list">\n                    {{i.text}}\n                </div>\n            </div>\n        </div>\n    </div> -->\n    <ul class="flex-ul" *ngIf=\'searching\'>\n        <li *ngFor="let i of allData" (click)=\'toPage(i.id)\'>\n            <!-- <div class="article-title">\n                <span [innerHTML]=\'i.title | wordPlace:keyword\'></span>\n            </div>\n            <div class="article-content">\n                {{i.content}}\n            </div>\n            <div class="article-remark">\n                <span>{{i.type}} </span>\n                <span>{{i.stage}}</span>\n            </div> -->\n            <div class="text-item"><span [innerHTML]=\'i.content | wordPlace:keyword\'></span></div>\n        </li>\n    </ul>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexChoicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inpu_userinfo_inpu_userinfo__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IndexChoicePage = /** @class */ (function () {
    function IndexChoicePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IndexChoicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IndexChoicePage');
        this.listData = [
            {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }, {
                flag: false,
                text: '病例一种'
            }
        ];
    };
    IndexChoicePage.prototype.showItem = function (index) {
        for (var i = 0; i < this.listData.length; i++) {
            this.listData[i].flag = false;
        }
        this.listData[index].flag = true;
    };
    IndexChoicePage.prototype.choiceGood = function () {
        for (var i = 0; i < this.listData.length; i++) {
            if (this.listData[i].flag) {
                console.log(this.listData[i].text);
            }
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__inpu_userinfo_inpu_userinfo__["a" /* InpuUserinfoPage */]);
    };
    IndexChoicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-index-choice',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/index-choice/index-choice.html"*/'<!--\n  Generated template for the IndexChoicePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>index-choice</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="page-title">\n      请选择您所患的病种\n    </div>\n    <div class="page-content">\n      <div class="item-box" *ngFor="let item of listData, let i = index" (click)="showItem(i)" [ngClass]="{\'choice-item\': item.flag}">\n        {{item.text}}\n      </div>\n    </div>\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'choiceGood()\'>\n        我选好了\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/index-choice/index-choice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], IndexChoicePage);
    return IndexChoicePage;
}());

//# sourceMappingURL=index-choice.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InpuUserinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__choice_position_choice_position__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InpuUserinfoPage = /** @class */ (function () {
    function InpuUserinfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.showRight = 0;
    }
    InpuUserinfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InpuUserinfoPage');
    };
    InpuUserinfoPage.prototype.choiceGood = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__choice_position_choice_position__["a" /* ChoicePositionPage */]);
    };
    InpuUserinfoPage.prototype.choiceMan = function () {
        this.showRight = 1;
    };
    InpuUserinfoPage.prototype.choiceWoman = function () {
        this.showRight = 2;
    };
    InpuUserinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-inpu-userinfo',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/inpu-userinfo/inpu-userinfo.html"*/'<!--\n  Generated template for the InpuUserinfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>inpu-userinfo</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="page-title">\n      请填写您的个人信息\n    </div>\n    <div class="page-content">\n      <div class="header-img">\n        <div [ngClass]="{\'img-item\':true, \'rect-circle\': showRight==1 ? true : false}" (click)=\'choiceMan()\'>\n          <img src="../../assets/imgs/man.png" alt="">\n          <div class="pos-choice" *ngIf=\'showRight==1\'>\n            <img src="../../assets/imgs/choice.png" alt="">\n          </div>\n          \n        </div>\n        <div [ngClass]="{\'img-item\':true, \'rect-circle\': showRight==2 ? true : false}" id=\'2\' (click)=\'choiceWoman()\'>\n          <img src="../../assets/imgs/women.png" alt="">\n          <div class="pos-choice" *ngIf=\'showRight==2\'>\n            <img src="../../assets/imgs/choice.png" alt="">\n          </div>\n        </div>\n      </div>\n\n      <div class="inp-text">\n        <input type="text" placeholder="请填写您的姓名">\n      </div>\n    </div>\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'choiceGood()\'>\n        下一步\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/inpu-userinfo/inpu-userinfo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], InpuUserinfoPage);
    return InpuUserinfoPage;
}());

//# sourceMappingURL=inpu-userinfo.js.map

/***/ }),

/***/ 133:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 133;

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/adjustment-plan/adjustment-plan.module": [
		445,
		21
	],
	"../pages/awaiting-audit/awaiting-audit.module": [
		446,
		20
	],
	"../pages/blood-pressure-data/blood-pressure-data.module": [
		447,
		19
	],
	"../pages/case-notice/case-notice.module": [
		448,
		18
	],
	"../pages/chat-group/chat-group.module": [
		449,
		17
	],
	"../pages/chat-info/chat-info.module": [
		450,
		16
	],
	"../pages/chat-record/chat-record.module": [
		451,
		15
	],
	"../pages/chat/chat.module": [
		452,
		14
	],
	"../pages/choice-item/choice-item.module": [
		453,
		13
	],
	"../pages/choice-position/choice-position.module": [
		454,
		12
	],
	"../pages/comfirm-plan/comfirm-plan.module": [
		455,
		11
	],
	"../pages/create-notice/create-notice.module": [
		456,
		10
	],
	"../pages/create-plan/create-plan.module": [
		457,
		9
	],
	"../pages/home-notice/home-notice.module": [
		458,
		8
	],
	"../pages/index-choice/index-choice.module": [
		459,
		7
	],
	"../pages/inpu-userinfo/inpu-userinfo.module": [
		460,
		6
	],
	"../pages/login/login.module": [
		461,
		5
	],
	"../pages/notice-detail/notice-detail.module": [
		462,
		4
	],
	"../pages/questionnaire/questionnaire.module": [
		463,
		3
	],
	"../pages/record-blood-pressure/record-blood-pressure.module": [
		464,
		2
	],
	"../pages/search/search.module": [
		465,
		1
	],
	"../pages/video-article/video-article.module": [
		466,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 175;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comfirm_plan_comfirm_plan__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__adjustment_plan_adjustment_plan__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PlanPage = /** @class */ (function () {
    function PlanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.slides = [];
        this.pageNumber = 2;
        this.slideClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selectedIndex = 0;
        this.val = false;
        this.evaData = [false, false, false];
    }
    PlanPage.prototype.ngOnInit = function () {
        this.weekData = [
            {
                flag: true,
                text: '日'
            }, {
                flag: false,
                text: '一'
            }, {
                flag: false,
                text: '二'
            }, {
                flag: false,
                text: '三'
            }, {
                flag: false,
                text: '四'
            }, {
                flag: false,
                text: '五'
            }, {
                flag: false,
                text: '六'
            }
        ];
        this.mySlideOptions = {
            loop: false,
            autoplay: false,
            initialSlide: 0,
            pager: false,
            slidesPerView: this.pageNumber,
            paginationHide: true,
            paginationClickable: true
        };
        this.slides = ['计划中', '已完成'];
    };
    PlanPage.prototype.onClick = function (index) {
        this.selectedIndex = index;
        this.slideClick.emit(index);
    };
    PlanPage.prototype.outpust = function (data) {
        console.log(data);
    };
    PlanPage.prototype.evaOut = function (data) {
        console.log(data);
        this.val = true;
    };
    PlanPage.prototype.confirmPlan = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__comfirm_plan_comfirm_plan__["a" /* ComfirmPlanPage */]);
    };
    PlanPage.prototype.editItem = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__adjustment_plan_adjustment_plan__["a" /* AdjustmentPlanPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("slides"),
        __metadata("design:type", Array)
    ], PlanPage.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("pageNumber"),
        __metadata("design:type", Number)
    ], PlanPage.prototype, "pageNumber", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])("slideClick"),
        __metadata("design:type", Object)
    ], PlanPage.prototype, "slideClick", void 0);
    PlanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-plan',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/plan/plan.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title>\n            计划\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="normal-page">\n        <div class=\'segment-box\'>\n            <ion-slides class="slide-title" [slidesPerView]="pageNumber" [pager]="false">\n                <ion-slide *ngFor="let slide of slides; let i = index;">\n                    <div (click)="onClick(i)">\n                        <span class="slide-title-unit"\n                            [ngClass]="{\'slide-title-active\': selectedIndex == i}">{{slide}}</span>\n                    </div>\n                </ion-slide>\n            </ion-slides>\n            <div class="switch-box" [ngSwitch]="selectedIndex">\n                <!-- <ion-list>\n\n                            <ion-item-sliding>\n            \n                                <ion-item>\n                                    <div class="ion-list">\n                                        <div>0</div>\n                                        <div>9</div>\n                                        <div>8</div>\n                                    </div>\n                                </ion-item>\n            \n                                <ion-item-options>\n                                    <button primary (click)=\'editItem(item)\'>修改</button>\n                                    <button style="background: #ED6161" (click)="removeItem(item)">删除</button>\n                                </ion-item-options>\n                            </ion-item-sliding>\n            \n                        </ion-list> -->\n                <ion-list *ngSwitchCase="0">\n\n                    <div class="switch-box-item">\n                        <div *ngFor=\'let item of weekData\' [ngClass]="{\'item-li\':true, \'choice\': item.flag}">\n                            <div>{{item.text}}</div>\n                            <div>3</div>\n                        </div>\n                    </div>\n                    <ion-item-sliding>\n                        <ion-item>\n                            <div class="switch-box-detail">\n                                <div class="detail-item">\n                                    <checkbox [values]=\'val\' (out)=\'outpust($event)\'></checkbox>\n                                    <div class="second-text">\n                                        <div>饭后散步</div>\n                                        <div>14：30</div>\n                                    </div>\n                                </div>\n                                <evaluate [datas]=\'evaData\' (out)=\'evaOut($event)\'></evaluate>\n                            </div>\n                        </ion-item>\n                        <ion-item-options>\n                            <button class="ion-item-btn" (click)=\'editItem(item)\'>修改</button>\n                        </ion-item-options>\n                    </ion-item-sliding>\n                </ion-list>\n                <ion-list *ngSwitchCase="1">\n                    <div class="switch-box-item-confirm">\n                        <div class="pos-div">\n                            3.2—3.9\n                        </div>\n                        <div class="confirm1">\n                            <div class="confirm1-left">\n                                <span>饭后散步</span>\n                                <span>14：30</span>\n                            </div>\n                            <div class="confirm1-right">\n                                <span>自信心：9分</span>\n                            </div>\n                        </div>\n                        <div class="confirm2">\n                            <div class="confirm2-item">日</div>\n                            <div class="confirm2-item"></div>\n                            <div class="confirm2-item"></div>\n                            <div class="confirm2-item"></div>\n                            <div class="confirm2-item"></div>\n                            <div class="confirm2-item"></div>\n                            <div class="confirm2-item"></div>\n                        </div>\n                        <div class="confirm3">\n                            <div class="confirm3-item">\n                                <div class="confirm3-item-checkbox confirm3-item-checkbox1"></div>\n                                <div class="confirm3-item-text">做得好</div>\n                            </div>\n                            <div class="confirm3-item">\n                                <div class="confirm3-item-checkbox confirm3-item-checkbox2"></div>\n                                <div class="confirm3-item-text">做得好</div>\n                            </div>\n                            <div class="confirm3-item">\n                                <div class="confirm3-item-checkbox confirm3-item-checkbox3"></div>\n                                <div class="confirm3-item-text">做得好</div>\n                            </div>\n                            <div class="confirm3-item">\n                                <div class="confirm3-item-checkbox confirm3-item-checkbox4"></div>\n                                <div class="confirm3-item-text">做得好</div>\n                            </div>\n                        </div>\n                        <div class="confirm4">\n                            <button ion-button (click)=\'editItem(1)\'>调整计划</button>\n                        </div>\n                    </div>\n                </ion-list>\n            </div>\n        </div>\n\n        <div class="footer">\n            <button class="button" (click)=\'confirmPlan()\'>完成本周计划</button>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/plan/plan.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PlanPage);
    return PlanPage;
}());

//# sourceMappingURL=plan.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__video_article_video_article__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ClassPage = /** @class */ (function () {
    function ClassPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.slides = [];
        this.pageNumber = 5;
        this.slideClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selectedIndex = 0;
    }
    ClassPage.prototype.ngOnInit = function () {
        this.mySlideOptions = {
            loop: false,
            autoplay: false,
            initialSlide: 0,
            pager: false,
            slidesPerView: this.pageNumber,
            paginationHide: true,
            paginationClickable: true
        };
        this.slides = ['文章', '音频', '视频'];
    };
    ClassPage.prototype.onClick = function (index) {
        this.selectedIndex = index;
        this.slideClick.emit(index);
    };
    ClassPage.prototype.toVideo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__video_article_video_article__["a" /* VideoArticlePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("slides"),
        __metadata("design:type", Array)
    ], ClassPage.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("pageNumber"),
        __metadata("design:type", Number)
    ], ClassPage.prototype, "pageNumber", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])("slideClick"),
        __metadata("design:type", Object)
    ], ClassPage.prototype, "slideClick", void 0);
    ClassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-class',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/class/class.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>\n      课程\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="normal-page">\n    <div class=\'segment-box\'>\n      <ion-slides class="slide-title" [slidesPerView]="pageNumber" [pager]="false">\n        <ion-slide *ngFor="let slide of slides; let i = index;">\n          <div (click)="onClick(i)">\n            <span class="slide-title-unit" [ngClass]="{\'slide-title-active\': selectedIndex == i}">{{slide}}</span>\n          </div>\n        </ion-slide>\n      </ion-slides>\n      <div class="switch-box" [ngSwitch]="selectedIndex">\n        <ion-list *ngSwitchCase="0">\n          <div class="switch-box-item"  (click)=\'toVideo()\'>\n            <div class="item">\n              <img src="../../assets/imgs/class-img.png" alt="">\n            </div>\n            <div class="item">\n              <div>如何进行体重管理</div>\n              <div>你的健康体重由自己管理</div>\n              <div>2019年1月20日</div>\n            </div>\n          </div>\n        </ion-list>\n        <ion-list *ngSwitchCase="1">\n          <div class="switch-box-item2" (click)=\'toVideo()\'>\n            <img class="bg" src="../../assets/imgs/audio-bg.png" alt="">\n            <div class="pos-center">\n              <img src="../../assets/imgs/pause.png" alt="">\n              <div>控制体重—你的生活管理</div>\n            </div>\n            <div class="pos-bottom">\n              <img src="../../assets/imgs/ripples.png" alt="">\n            </div>\n          </div>\n        </ion-list>\n        <ion-list *ngSwitchCase="2">\n          <div class="switch-box-item2"  (click)=\'toVideo()\'>\n            <img class="bg" src="../../assets/imgs/audio-bg.png" alt="">\n            <div class="pos-center">\n              <img src="../../assets/imgs/pause.png" alt="">\n              <div>控制体重—你的生活管理</div>\n            </div>\n          </div>\n        </ion-list>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/class/class.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ClassPage);
    return ClassPage;
}());

//# sourceMappingURL=class.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_chat__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__case_notice_case_notice__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_notice_home_notice__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chat_group_chat_group__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__record_blood_pressure_record_blood_pressure__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__questionnaire_questionnaire__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.canShow = false;
        this.pushModal = false;
    }
    HomePage.prototype.ngOnInit = function () {
        this.canShow = true;
        this.listData = [
            {
                title: '公告',
                img: '../../assets/imgs/news.png'
            }, {
                title: '病种公告',
                img: '../../assets/imgs/news2.png'
            }, {
                title: '小组',
                img: '../../assets/imgs/group.png'
            }
        ];
        this.pushModal = true;
        this.modalTextArr = [
            {
                title: '计划提醒',
                text: '您要开始做本周的计划了，请尽快做出你自己的计划哦！为了更好的自我管理，请尽快进行哦！'
            }, {
                title: '计划提醒',
                text: '您今天的计划完成了嘛？快去完成今天的计划吧，今天的计划是饭后10分钟散步！'
            }, {
                title: '问卷调查',
                text: '为了更好的进行自我管理，我们计划为您提供个性化的管理，请填写相关问卷'
            }
        ];
    };
    HomePage.prototype.toChat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_chat__["a" /* ChatPage */]);
    };
    HomePage.prototype.toClass = function () {
        this.navCtrl.parent.select(1);
    };
    HomePage.prototype.toNotice = function (index) {
        if (index == 0) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_notice_home_notice__["a" /* HomeNoticePage */]);
        }
        else if (index == 1) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__case_notice_case_notice__["a" /* CaseNoticePage */]);
        }
        else if (index == 2) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chat_group_chat_group__["a" /* ChatGroupPage */]);
        }
    };
    HomePage.prototype.toRecordBlood = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__record_blood_pressure_record_blood_pressure__["a" /* RecordBloodPressurePage */]);
    };
    HomePage.prototype.checkConform = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__questionnaire_questionnaire__["a" /* QuestionnairePage */]);
    };
    HomePage.prototype.closeModal = function () {
        this.pushModal = false;
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>首页</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="normal-page">\n    <div class="pos-top" *ngIf=\'!canShow\'>\n      <div class="header-img">\n        <img src="../../assets/imgs/women.png" alt="">\n      </div>\n      <div>你已申请加入徐汇糖尿病--A组</div>\n      <div>请等待审核</div>\n    </div>\n    <div class="pos-top-row" *ngIf=\'canShow\'>\n      <div class=\'row-left-item\'>\n        <div>徐汇四和花园居委—糖尿病A组</div>\n        <div>活动时间：每周三14：30</div>\n        <div>成员：20人</div>\n      </div>\n      <div class="header-img">\n        <img src="../../assets/imgs/women.png" alt="">\n        <span>陆长风</span>\n      </div>\n    </div>\n    <div class="content-box">\n      <div class="content" *ngIf=\'!canShow\'>\n        <div class="flex-box">\n          <div class="left-con">\n            <div>管理员正在努力审核</div>\n            <div>请耐心等待哦！</div>\n          </div>\n          <div class="right-con">\n            <img src="../../assets/imgs/shenhe-icon.png" alt="">\n          </div>\n        </div>\n        <div class="btn">\n          <button ion-button (click)=\'toClass()\'>查看课程</button>\n        </div>\n      </div>\n      <div class="content" *ngIf=\'canShow\'>\n        <div class="list-box">\n          <div class="list-item" *ngFor=\'let item of listData, let i = index\' (click)=\'toNotice(i)\'>\n            <img src="{{item.img}}" alt="">\n            <span>{{item.title}}</span>\n          </div>\n        </div>\n        <div class="need-do">\n          <div class=\'need-do-title\'>今日待办</div>\n          <div class="need-do-box">\n            <div class="need-list-item">\n              <img src="../../assets/imgs/yaliji.png" alt="">\n            </div>\n            <div class="need-list-item-center">\n              <div>量血压</div>\n              <div>量下今日血压吧！</div>\n            </div>\n            <div class="need-list-item-end" (click)=\'toRecordBlood()\'>\n              <span>完成</span>\n            </div>\n          </div>\n        </div>\n        <div class="btn">\n          <button ion-button (click)=\'toChat()\'>群组聊天</button>\n        </div>\n      </div>\n    </div>\n\n    <div class="pos-modal" *ngIf=\'pushModal\'>\n        <div class="pos-dialog">\n            <div class="pos-content">\n                <div class="modal-header">\n                    <img src="../../assets/imgs/notice-line.png" alt="">\n                    <span class="font-color1">{{modalTextArr[0].title}}</span>\n                    <img src="../../assets/imgs/notice-line.png" alt="">\n                </div>\n                <div class="modal-center font-color4">\n                    {{modalTextArr[0].text}}\n                </div>\n                <div class="modal-footer">\n                    <button ion-button (click)=\'checkConform()\'>去完成</button>\n                </div>\n            </div>\n            <div class="modal-close" (click)=\'closeModal()\'>\n                <img src="../../../assets/imgs/modal-close.png" alt="">\n            </div>\n        </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MinePage = /** @class */ (function () {
    function MinePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    MinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-mine',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/mine/mine.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title>\n            我的\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="normal-page">\n        \n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/mine/mine.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], MinePage);
    return MinePage;
}());

//# sourceMappingURL=mine.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_choice_index_choice__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.isHide = false;
        this.showPhone = true;
        this.codeString = '获取验证码';
        this.codeNum = "";
    }
    LoginPage.prototype.ngOnInit = function () {
        console.log('LoginPage');
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toRegister = function () {
        console.log("toRegister");
    };
    LoginPage.prototype.changeLoginType = function () {
        if (this.showPhone) {
            this.showPhone = false;
        }
        else {
            this.showPhone = true;
        }
    };
    LoginPage.prototype.getCode = function () {
        var _this = this;
        clearInterval(this.timer);
        console.log("获取验证码");
        this.codeString = "重新获取";
        this.codeNum = 60;
        this.timer = setInterval(function () {
            if (_this.codeNum <= 0) {
                _this.codeNum = 0;
                clearInterval(_this.timer);
            }
            else {
                _this.codeNum--;
            }
            console.log(_this.codeNum);
        }, 1000);
    };
    LoginPage.prototype.toForgetPwd = function () {
        console.log("toForgetPwd");
        // this.navCtrl.push(ForgetPwdPage);
    };
    LoginPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index_choice_index_choice__["a" /* IndexChoicePage */]);
    };
    LoginPage.prototype.focusInput = function () {
        this.isHide = true;
    };
    LoginPage.prototype.blurInput = function () {
        this.isHide = false;
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <!-- <ion-navbar>\n    <ion-title>login</ion-title>\n  </ion-navbar> -->\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="pos-bg">\n      <img src="../../assets/imgs/login-bg.png" alt="">\n    </div>\n    <div class="flex-page login-page">\n      <div class="login-page-box text">\n        <img src=\'../../assets/imgs/logo.png\' />\n        <span>app名字</span>\n      </div>\n      <div class="ion-input-box margin-top-40" *ngIf=\'showPhone\'>\n        <input type="text" placeholder="用户名" (focus)="focusInput()" (blur)="blurInput()">\n      </div>\n      <div class="ion-input-box" *ngIf=\'showPhone\'>\n        <input type="password" placeholder="密码" (focus)="focusInput()" (blur)="blurInput()">\n      </div>\n      <div class="ion-input-box margin-top-40" *ngIf=\'!showPhone\'>\n        <input type="text" placeholder="用户名" [ngModel]=\'username\' (focus)="focusInput()" (blur)="blurInput()">\n      </div>\n      <div class="ion-input-box pos-relative margin-top-40" *ngIf=\'!showPhone\'>\n        <input type="text" placeholder="请输入验证码" [ngModel]=\'code\' (ngModelChange)=\'checkCode($event)\' (focus)="focusInput()"\n          (blur)="blurInput()">\n        <span class="pos-code" (click)="getCode()"><span *ngIf=\'codeNum > 0 && codeNum <= 60\'> ({{codeNum}}S) </span>{{codeString}}</span>\n      </div>\n      <div class="ion-input-box">\n        <div class="btn-login text" (click)=\'login()\'>登录</div>\n      </div>\n      <div class="footer">\n        <div class="text" (click)=\'changeLoginType()\'>账号登录</div>\n      </div>\n\n      <div class="page-footer-register" *ngIf=\'!isHide\'>\n        <div class="text">\n          自我管理APP的solgan\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(376);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_components_module__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_signaturepad__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_signaturepad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_signaturepad__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_plan_plan__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_class_class__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_mine_mine__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_login_login__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_index_choice_index_choice__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_inpu_userinfo_inpu_userinfo__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_choice_position_choice_position__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_search_search__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_choice_item_choice_item__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_awaiting_audit_awaiting_audit__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_chat_info_chat_info__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_chat_group_chat_group__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_chat_record_chat_record__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_case_notice_case_notice__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_home_notice_home_notice__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_notice_detail_notice_detail__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_create_notice_create_notice__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_record_blood_pressure_record_blood_pressure__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_blood_pressure_data_blood_pressure_data__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_questionnaire_questionnaire__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_video_article_video_article__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_comfirm_plan_comfirm_plan__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_create_plan_create_plan__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_adjustment_plan_adjustment_plan__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_status_bar__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_splash_screen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_http_server_http_server__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pipes_pipes_module__ = __webpack_require__(440);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// 组件引用，因为plan页面本就是一个组件,所以选择直接在app.module中引入

// 滑动选择插件  需要先npm install ion-multi-picker安装包 在imports中添加

// 签名插件

// 首页的tab-bar





// 登录

// 聊天界面

// 进入选择病例

// 填写个人信息

// 选择所处的位置

// 搜索居委

// 选择活动小组

// 等待管理员审核

// 聊天信息

// 聊天群组

// 聊天记录

// 病例公告

// 首页公告

// 公告详情

// 创建公告

// 记录血压

// 血压数据

// 问卷调查

// 视频文章

// 完成计划

// 新建计划

// 调整计划




// pipe

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_plan_plan__["a" /* PlanPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_class_class__["a" /* ClassPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_mine_mine__["a" /* MinePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_index_choice_index_choice__["a" /* IndexChoicePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_inpu_userinfo_inpu_userinfo__["a" /* InpuUserinfoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_choice_position_choice_position__["a" /* ChoicePositionPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_choice_item_choice_item__["a" /* ChoiceItemPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_awaiting_audit_awaiting_audit__["a" /* AwaitingAuditPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_chat_info_chat_info__["a" /* ChatInfoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_chat_group_chat_group__["a" /* ChatGroupPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_chat_record_chat_record__["a" /* ChatRecordPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_case_notice_case_notice__["a" /* CaseNoticePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_home_notice_home_notice__["a" /* HomeNoticePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_notice_detail_notice_detail__["a" /* NoticeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_create_notice_create_notice__["a" /* CreateNoticePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_record_blood_pressure_record_blood_pressure__["a" /* RecordBloodPressurePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_blood_pressure_data_blood_pressure_data__["a" /* BloodPressureDataPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_questionnaire_questionnaire__["a" /* QuestionnairePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_video_article_video_article__["a" /* VideoArticlePage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_comfirm_plan_comfirm_plan__["a" /* ComfirmPlanPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_create_plan_create_plan__["a" /* CreatePlanPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_adjustment_plan_adjustment_plan__["a" /* AdjustmentPlanPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_6_angular2_signaturepad__["SignaturePadModule"],
                __WEBPACK_IMPORTED_MODULE_37__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__["MultiPickerModule"],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    iconMode: 'ios',
                    mode: 'ios',
                    tabsHideOnSubPages: 'true',
                    backButtonText: "" /*修改返回按钮为返回（默认是）*/
                }, {
                    links: [
                        { loadChildren: '../pages/adjustment-plan/adjustment-plan.module#AdjustmentPlanPageModule', name: 'AdjustmentPlanPage', segment: 'adjustment-plan', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/awaiting-audit/awaiting-audit.module#AwaitingAuditPageModule', name: 'AwaitingAuditPage', segment: 'awaiting-audit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/blood-pressure-data/blood-pressure-data.module#BloodPressureDataPageModule', name: 'BloodPressureDataPage', segment: 'blood-pressure-data', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/case-notice/case-notice.module#CaseNoticePageModule', name: 'CaseNoticePage', segment: 'case-notice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-group/chat-group.module#ChatGroupPageModule', name: 'ChatGroupPage', segment: 'chat-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-info/chat-info.module#ChatInfoPageModule', name: 'ChatInfoPage', segment: 'chat-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-record/chat-record.module#ChatRecordPageModule', name: 'ChatRecordPage', segment: 'chat-record', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/choice-item/choice-item.module#ChoiceItemPageModule', name: 'ChoiceItemPage', segment: 'choice-item', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/choice-position/choice-position.module#ChoicePositionPageModule', name: 'ChoicePositionPage', segment: 'choice-position', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comfirm-plan/comfirm-plan.module#ComfirmPlanPageModule', name: 'ComfirmPlanPage', segment: 'comfirm-plan', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-notice/create-notice.module#CreateNoticePageModule', name: 'CreateNoticePage', segment: 'create-notice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-plan/create-plan.module#CreatePlanPageModule', name: 'CreatePlanPage', segment: 'create-plan', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home-notice/home-notice.module#HomeNoticePageModule', name: 'HomeNoticePage', segment: 'home-notice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/index-choice/index-choice.module#IndexChoicePageModule', name: 'IndexChoicePage', segment: 'index-choice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inpu-userinfo/inpu-userinfo.module#InpuUserinfoPageModule', name: 'InpuUserinfoPage', segment: 'inpu-userinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notice-detail/notice-detail.module#NoticeDetailPageModule', name: 'NoticeDetailPage', segment: 'notice-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/questionnaire/questionnaire.module#QuestionnairePageModule', name: 'QuestionnairePage', segment: 'questionnaire', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/record-blood-pressure/record-blood-pressure.module#RecordBloodPressurePageModule', name: 'RecordBloodPressurePage', segment: 'record-blood-pressure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/video-article/video-article.module#VideoArticlePageModule', name: 'VideoArticlePage', segment: 'video-article', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_plan_plan__["a" /* PlanPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_class_class__["a" /* ClassPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_mine_mine__["a" /* MinePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_index_choice_index_choice__["a" /* IndexChoicePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_inpu_userinfo_inpu_userinfo__["a" /* InpuUserinfoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_choice_position_choice_position__["a" /* ChoicePositionPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_choice_item_choice_item__["a" /* ChoiceItemPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_awaiting_audit_awaiting_audit__["a" /* AwaitingAuditPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_chat_info_chat_info__["a" /* ChatInfoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_chat_group_chat_group__["a" /* ChatGroupPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_chat_record_chat_record__["a" /* ChatRecordPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_case_notice_case_notice__["a" /* CaseNoticePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_home_notice_home_notice__["a" /* HomeNoticePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_notice_detail_notice_detail__["a" /* NoticeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_create_notice_create_notice__["a" /* CreateNoticePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_record_blood_pressure_record_blood_pressure__["a" /* RecordBloodPressurePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_blood_pressure_data_blood_pressure_data__["a" /* BloodPressureDataPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_questionnaire_questionnaire__["a" /* QuestionnairePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_video_article_video_article__["a" /* VideoArticlePage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_comfirm_plan_comfirm_plan__["a" /* ComfirmPlanPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_create_plan_create_plan__["a" /* CreatePlanPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_adjustment_plan_adjustment_plan__["a" /* AdjustmentPlanPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_36__providers_http_server_http_server__["a" /* HttpServerProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { LoginPage } from '../pages/login/login';

// import { ChatPage } from '../pages/chat/chat';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkbox_checkbox__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__evaluate_evaluate__ = __webpack_require__(429);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__checkbox_checkbox__["a" /* CheckboxComponent */],
                __WEBPACK_IMPORTED_MODULE_3__evaluate_evaluate__["a" /* EvaluateComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__checkbox_checkbox__["a" /* CheckboxComponent */],
                __WEBPACK_IMPORTED_MODULE_3__evaluate_evaluate__["a" /* EvaluateComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckboxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CheckboxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.isChecked = false;
        this.out = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"];
        console.log('Hello CheckboxComponent Component');
        this.text = 'Hello World';
    }
    CheckboxComponent.prototype.ngOnInit = function () {
        console.log(this.values);
        this.values = false;
    };
    CheckboxComponent.prototype.checkCli = function () {
        if (this.values) {
            return false;
        }
        if (this.isChecked) {
            this.isChecked = false;
        }
        else {
            this.isChecked = true;
        }
        this.out.emit(this.isChecked);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CheckboxComponent.prototype, "values", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"])
    ], CheckboxComponent.prototype, "out", void 0);
    CheckboxComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'checkbox',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/components/checkbox/checkbox.html"*/'<!-- Generated template for the CheckboxComponent component -->\n<div>\n  <div [ngClass]="{\'checkbox\': true, \'is-over\': values}" (click)=\'checkCli()\'>\n    <div class="is-checked" *ngIf=\'isChecked\'>\n      <img src="../../assets/imgs/checked.png" alt="">\n    </div>\n  </div>\n</div>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/components/checkbox/checkbox.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavController"]])
    ], CheckboxComponent);
    return CheckboxComponent;
}());

//# sourceMappingURL=checkbox.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EvaluateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the EvaluateComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var EvaluateComponent = /** @class */ (function () {
    function EvaluateComponent() {
        this.out = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        console.log('Hello EvaluateComponent Component');
        this.listData = [
            {
                isChecked: false,
                img: '../../assets/imgs/lian1.png',
                imgChecked: '../../assets/imgs/lian1-checked.png'
            }, {
                isChecked: false,
                img: '../../assets/imgs/lian2.png',
                imgChecked: '../../assets/imgs/lian2-checked.png'
            }, {
                isChecked: false,
                img: '../../assets/imgs/lian3.png',
                imgChecked: '../../assets/imgs/lian3-checked.png'
            }
        ];
    }
    EvaluateComponent.prototype.ngOnInit = function () {
        console.log(this.datas);
    };
    EvaluateComponent.prototype.checkedCli = function (index) {
        for (var i = 0; i < this.listData.length; i++) {
            this.listData[i].isChecked = false;
        }
        this.listData[index].isChecked = true;
        this.out.emit(this.listData);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], EvaluateComponent.prototype, "datas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], EvaluateComponent.prototype, "out", void 0);
    EvaluateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'evaluate',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/components/evaluate/evaluate.html"*/'<!-- Generated template for the EvaluateComponent component -->\n<div>\n  <div class="evaluate">\n      <div *ngFor=\'let item of listData, let i = index\' (click)=\'checkedCli(i)\'>\n          <img *ngIf=\'!item.isChecked\' src="{{item.img}}" alt="">\n          <img *ngIf=\'item.isChecked\' src="{{item.imgChecked}}" alt="">\n        </div>\n  </div>\n</div>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/components/evaluate/evaluate.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], EvaluateComponent);
    return EvaluateComponent;
}());

//# sourceMappingURL=evaluate.js.map

/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpServerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the HttpServerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HttpServerProvider = /** @class */ (function () {
    function HttpServerProvider(http) {
        this.http = http;
        console.log('Hello HttpServerProvider Provider');
    }
    HttpServerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], HttpServerProvider);
    return HttpServerProvider;
}());

//# sourceMappingURL=http-server.js.map

/***/ }),

/***/ 440:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__moment_moment__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__word_place_word_place__ = __webpack_require__(444);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__moment_moment__["a" /* MomentPipe */],
                __WEBPACK_IMPORTED_MODULE_2__word_place_word_place__["a" /* WordPlacePipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__moment_moment__["a" /* MomentPipe */],
                __WEBPACK_IMPORTED_MODULE_2__word_place_word_place__["a" /* WordPlacePipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MomentPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * Generated class for the MomentPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var MomentPipe = /** @class */ (function () {
    function MomentPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    MomentPipe.prototype.transform = function (value, args) {
        args = args || '';
        return args === 'ago' ? __WEBPACK_IMPORTED_MODULE_1_moment___default()(value).fromNow() : __WEBPACK_IMPORTED_MODULE_1_moment___default()(value).format(args);
    };
    MomentPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'moment',
        })
    ], MomentPipe);
    return MomentPipe;
}());

//# sourceMappingURL=moment.js.map

/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 226,
	"./af.js": 226,
	"./ar": 227,
	"./ar-dz": 228,
	"./ar-dz.js": 228,
	"./ar-kw": 229,
	"./ar-kw.js": 229,
	"./ar-ly": 230,
	"./ar-ly.js": 230,
	"./ar-ma": 231,
	"./ar-ma.js": 231,
	"./ar-sa": 232,
	"./ar-sa.js": 232,
	"./ar-tn": 233,
	"./ar-tn.js": 233,
	"./ar.js": 227,
	"./az": 234,
	"./az.js": 234,
	"./be": 235,
	"./be.js": 235,
	"./bg": 236,
	"./bg.js": 236,
	"./bm": 237,
	"./bm.js": 237,
	"./bn": 238,
	"./bn.js": 238,
	"./bo": 239,
	"./bo.js": 239,
	"./br": 240,
	"./br.js": 240,
	"./bs": 241,
	"./bs.js": 241,
	"./ca": 242,
	"./ca.js": 242,
	"./cs": 243,
	"./cs.js": 243,
	"./cv": 244,
	"./cv.js": 244,
	"./cy": 245,
	"./cy.js": 245,
	"./da": 246,
	"./da.js": 246,
	"./de": 247,
	"./de-at": 248,
	"./de-at.js": 248,
	"./de-ch": 249,
	"./de-ch.js": 249,
	"./de.js": 247,
	"./dv": 250,
	"./dv.js": 250,
	"./el": 251,
	"./el.js": 251,
	"./en-SG": 252,
	"./en-SG.js": 252,
	"./en-au": 253,
	"./en-au.js": 253,
	"./en-ca": 254,
	"./en-ca.js": 254,
	"./en-gb": 255,
	"./en-gb.js": 255,
	"./en-ie": 256,
	"./en-ie.js": 256,
	"./en-il": 257,
	"./en-il.js": 257,
	"./en-nz": 258,
	"./en-nz.js": 258,
	"./eo": 259,
	"./eo.js": 259,
	"./es": 260,
	"./es-do": 261,
	"./es-do.js": 261,
	"./es-us": 262,
	"./es-us.js": 262,
	"./es.js": 260,
	"./et": 263,
	"./et.js": 263,
	"./eu": 264,
	"./eu.js": 264,
	"./fa": 265,
	"./fa.js": 265,
	"./fi": 266,
	"./fi.js": 266,
	"./fo": 267,
	"./fo.js": 267,
	"./fr": 268,
	"./fr-ca": 269,
	"./fr-ca.js": 269,
	"./fr-ch": 270,
	"./fr-ch.js": 270,
	"./fr.js": 268,
	"./fy": 271,
	"./fy.js": 271,
	"./ga": 272,
	"./ga.js": 272,
	"./gd": 273,
	"./gd.js": 273,
	"./gl": 274,
	"./gl.js": 274,
	"./gom-latn": 275,
	"./gom-latn.js": 275,
	"./gu": 276,
	"./gu.js": 276,
	"./he": 277,
	"./he.js": 277,
	"./hi": 278,
	"./hi.js": 278,
	"./hr": 279,
	"./hr.js": 279,
	"./hu": 280,
	"./hu.js": 280,
	"./hy-am": 281,
	"./hy-am.js": 281,
	"./id": 282,
	"./id.js": 282,
	"./is": 283,
	"./is.js": 283,
	"./it": 284,
	"./it-ch": 285,
	"./it-ch.js": 285,
	"./it.js": 284,
	"./ja": 286,
	"./ja.js": 286,
	"./jv": 287,
	"./jv.js": 287,
	"./ka": 288,
	"./ka.js": 288,
	"./kk": 289,
	"./kk.js": 289,
	"./km": 290,
	"./km.js": 290,
	"./kn": 291,
	"./kn.js": 291,
	"./ko": 292,
	"./ko.js": 292,
	"./ku": 293,
	"./ku.js": 293,
	"./ky": 294,
	"./ky.js": 294,
	"./lb": 295,
	"./lb.js": 295,
	"./lo": 296,
	"./lo.js": 296,
	"./lt": 297,
	"./lt.js": 297,
	"./lv": 298,
	"./lv.js": 298,
	"./me": 299,
	"./me.js": 299,
	"./mi": 300,
	"./mi.js": 300,
	"./mk": 301,
	"./mk.js": 301,
	"./ml": 302,
	"./ml.js": 302,
	"./mn": 303,
	"./mn.js": 303,
	"./mr": 304,
	"./mr.js": 304,
	"./ms": 305,
	"./ms-my": 306,
	"./ms-my.js": 306,
	"./ms.js": 305,
	"./mt": 307,
	"./mt.js": 307,
	"./my": 308,
	"./my.js": 308,
	"./nb": 309,
	"./nb.js": 309,
	"./ne": 310,
	"./ne.js": 310,
	"./nl": 311,
	"./nl-be": 312,
	"./nl-be.js": 312,
	"./nl.js": 311,
	"./nn": 313,
	"./nn.js": 313,
	"./pa-in": 314,
	"./pa-in.js": 314,
	"./pl": 315,
	"./pl.js": 315,
	"./pt": 316,
	"./pt-br": 317,
	"./pt-br.js": 317,
	"./pt.js": 316,
	"./ro": 318,
	"./ro.js": 318,
	"./ru": 319,
	"./ru.js": 319,
	"./sd": 320,
	"./sd.js": 320,
	"./se": 321,
	"./se.js": 321,
	"./si": 322,
	"./si.js": 322,
	"./sk": 323,
	"./sk.js": 323,
	"./sl": 324,
	"./sl.js": 324,
	"./sq": 325,
	"./sq.js": 325,
	"./sr": 326,
	"./sr-cyrl": 327,
	"./sr-cyrl.js": 327,
	"./sr.js": 326,
	"./ss": 328,
	"./ss.js": 328,
	"./sv": 329,
	"./sv.js": 329,
	"./sw": 330,
	"./sw.js": 330,
	"./ta": 331,
	"./ta.js": 331,
	"./te": 332,
	"./te.js": 332,
	"./tet": 333,
	"./tet.js": 333,
	"./tg": 334,
	"./tg.js": 334,
	"./th": 335,
	"./th.js": 335,
	"./tl-ph": 336,
	"./tl-ph.js": 336,
	"./tlh": 337,
	"./tlh.js": 337,
	"./tr": 338,
	"./tr.js": 338,
	"./tzl": 339,
	"./tzl.js": 339,
	"./tzm": 340,
	"./tzm-latn": 341,
	"./tzm-latn.js": 341,
	"./tzm.js": 340,
	"./ug-cn": 342,
	"./ug-cn.js": 342,
	"./uk": 343,
	"./uk.js": 343,
	"./ur": 344,
	"./ur.js": 344,
	"./uz": 345,
	"./uz-latn": 346,
	"./uz-latn.js": 346,
	"./uz.js": 345,
	"./vi": 347,
	"./vi.js": 347,
	"./x-pseudo": 348,
	"./x-pseudo.js": 348,
	"./yo": 349,
	"./yo.js": 349,
	"./zh-cn": 350,
	"./zh-cn.js": 350,
	"./zh-hk": 351,
	"./zh-hk.js": 351,
	"./zh-tw": 352,
	"./zh-tw.js": 352
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 443;

/***/ }),

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WordPlacePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the WordPlacePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var WordPlacePipe = /** @class */ (function () {
    function WordPlacePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    WordPlacePipe.prototype.transform = function (input, word) {
        if (!word)
            return input;
        var result = input.replace(word, "<span style='color: #58B5FC;'>" + word + "</span>");
        return this.sanitizer.bypassSecurityTrustHtml(result);
    };
    WordPlacePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Pipe"])({
            name: 'wordPlace',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["c" /* DomSanitizer */]])
    ], WordPlacePipe);
    return WordPlacePipe;
}());

//# sourceMappingURL=word-place.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ChatGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatGroupPage = /** @class */ (function () {
    function ChatGroupPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChatGroupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatGroupPage');
        this.listData = [
            {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }, {
                img: '../../assets/imgs/man.png',
                name: '王爱华',
                title: '组长'
            }
        ];
    };
    ChatGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat-group',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-group/chat-group.html"*/'<!--\n  Generated template for the ChoiceItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color=\'primary\'>\n    <ion-title>小组</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="normal-page">\n    <div class="page-content">\n      <div class="pos-top" *ngIf=\'!canShow\'>\n        <div>\n            徐汇四和花园居委糖尿病—A组\n        </div>\n        <div>活动时间：每周三14：30--A组</div>\n        <div>地址：徐汇区中山西路街道</div>\n      </div>\n      <div class="list-content">\n        <div class="title">列表成员: 20人</div>\n        <ul>\n          <li *ngFor=\'let item of listData\'>\n            <div class="header-img">\n              <img src="{{item.img}}" alt="">\n            </div>\n            <div class="detail">\n              {{item.title}}\n            </div>\n            <div class="user-name">\n                {{item.name}}\n            </div>\n          </li>\n        </ul>\n      </div>\n    </div>\n\n    <div class="footer-btn">\n      <div class="choice-btn" (click)=\'choiceQuit()\'>\n        群组聊天\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/chat-group/chat-group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChatGroupPage);
    return ChatGroupPage;
}());

//# sourceMappingURL=chat-group.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plan_plan__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__class_class__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mine_mine__ = __webpack_require__(180);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.canShow = true;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__class_class__["a" /* ClassPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__plan_plan__["a" /* PlanPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_5__mine_mine__["a" /* MinePage */];
    }
    TabsPage.prototype.ionViewDidEnter = function () {
        // this.tabRef.select(2);
    };
    TabsPage.prototype.ngOnInit = function () {
        // this.canShow = true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Tabs"])
    ], TabsPage.prototype, "tabRef", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/tabs/tabs.html"*/'<ion-tabs #myTabs>\n  <ion-tab [root]="tab1Root" tabTitle="首页" tabIcon="tab-home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="课程" tabIcon="tab-class"></ion-tab>\n  <ion-tab *ngIf=\'canShow\' [root]="tab3Root" tabTitle="计划" tabIcon="tab-plan"></ion-tab>\n  <ion-tab *ngIf=\'canShow\' [root]="tab4Root" tabTitle="我的" tabIcon="tab-mine"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/selfManagementApp/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ })

},[355]);
//# sourceMappingURL=main.js.map