import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// 组件引用，因为plan页面本就是一个组件,所以选择直接在app.module中引入
import { ComponentsModule } from '../components/components.module';

// 滑动选择插件  需要先npm install ion-multi-picker安装包 在imports中添加
import { MultiPickerModule } from 'ion-multi-picker';
// 签名插件
import { SignaturePadModule } from 'angular2-signaturepad';

// 首页的tab-bar
import { PlanPage } from '../pages/plan/plan';
import { ClassPage } from '../pages/class/class';
import { HomePage } from '../pages/home/home';
import { MinePage } from '../pages/mine/mine';
import { TabsPage } from '../pages/tabs/tabs';
// 登录
import { LoginPage } from '../pages/login/login';
// 聊天界面
import { ChatPage } from '../pages/chat/chat';
// 进入选择病例
import { IndexChoicePage } from '../pages/index-choice/index-choice';
// 填写个人信息
import { InpuUserinfoPage } from '../pages/inpu-userinfo/inpu-userinfo';
// 选择所处的位置
import { ChoicePositionPage } from '../pages/choice-position/choice-position';
// 搜索居委
import { SearchPage } from '../pages/search/search';
// 选择活动小组
import { ChoiceItemPage } from '../pages/choice-item/choice-item';
// 等待管理员审核
import { AwaitingAuditPage } from '../pages/awaiting-audit/awaiting-audit';
// 聊天信息
import { ChatInfoPage } from '../pages/chat-info/chat-info';
// 聊天群组
import { ChatGroupPage } from '../pages/chat-group/chat-group';
// 聊天记录
import { ChatRecordPage } from '../pages/chat-record/chat-record';
// 病例公告
import { CaseNoticePage } from '../pages/case-notice/case-notice';
// 首页公告
import { HomeNoticePage } from '../pages/home-notice/home-notice';
// 公告详情
import { NoticeDetailPage } from '../pages/notice-detail/notice-detail';
// 创建公告
import { CreateNoticePage } from '../pages/create-notice/create-notice';
// 记录血压
import { RecordBloodPressurePage } from '../pages/record-blood-pressure/record-blood-pressure';
// 血压数据
import { BloodPressureDataPage } from '../pages/blood-pressure-data/blood-pressure-data';
// 问卷调查
import { QuestionnairePage } from '../pages/questionnaire/questionnaire';
// 视频文章
import { VideoArticlePage } from '../pages/video-article/video-article';
// 完成计划
import { ComfirmPlanPage } from '../pages/comfirm-plan/comfirm-plan';
// 新建计划
import { CreatePlanPage } from '../pages/create-plan/create-plan';
// 调整计划
import { AdjustmentPlanPage } from '../pages/adjustment-plan/adjustment-plan';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpServerProvider } from '../providers/http-server/http-server';

// pipe
import { PipesModule } from '../pipes/pipes.module'

@NgModule({
  declarations: [
    MyApp,
    PlanPage,
    ClassPage,
    HomePage,
    MinePage,
    TabsPage,
    LoginPage,
    ChatPage,
    IndexChoicePage,
    InpuUserinfoPage,
    ChoicePositionPage,
    SearchPage,
    ChoiceItemPage,
    AwaitingAuditPage,
    ChatInfoPage,
    ChatGroupPage,
    ChatRecordPage,
    CaseNoticePage, 
    HomeNoticePage,
    NoticeDetailPage,
    CreateNoticePage,
    RecordBloodPressurePage,
    BloodPressureDataPage,
    QuestionnairePage,
    VideoArticlePage,
    ComfirmPlanPage,
    CreatePlanPage,
    AdjustmentPlanPage,
  ],
  imports: [
    ComponentsModule,
    SignaturePadModule,
    PipesModule,
    BrowserModule,
    MultiPickerModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      mode: 'ios',
      tabsHideOnSubPages: 'true',        //隐藏全部子页面tabs
      backButtonText: ""  /*修改返回按钮为返回（默认是）*/
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlanPage,
    ClassPage,
    HomePage,
    MinePage,
    TabsPage,
    LoginPage,
    ChatPage,
    IndexChoicePage,
    InpuUserinfoPage,
    ChoicePositionPage,
    SearchPage,
    ChoiceItemPage,
    AwaitingAuditPage,
    ChatInfoPage,
    ChatGroupPage,
    ChatRecordPage,
    CaseNoticePage, 
    HomeNoticePage,
    NoticeDetailPage,
    CreateNoticePage,
    RecordBloodPressurePage,
    BloodPressureDataPage,
    QuestionnairePage,
    VideoArticlePage,
    ComfirmPlanPage,
    CreatePlanPage,
    AdjustmentPlanPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpServerProvider
  ]
})
export class AppModule {}
