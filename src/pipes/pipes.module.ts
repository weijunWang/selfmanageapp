import { NgModule } from '@angular/core';
import { MomentPipe } from './moment/moment';
import { WordPlacePipe } from './word-place/word-place';
@NgModule({
	declarations: [MomentPipe,
    WordPlacePipe],
	imports: [],
	exports: [MomentPipe,
    WordPlacePipe]
})
export class PipesModule {}
