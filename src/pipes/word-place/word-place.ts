import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the WordPlacePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'wordPlace',
})
export class WordPlacePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer){}
  transform(input: string, word) {
    if (!word) return input;
    var result = input.replace(word, "<span style='color: #58B5FC;'>" + word + "</span>");
    return this.sanitizer.bypassSecurityTrustHtml(result);
  }
}
