import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComfirmPlanPage } from './comfirm-plan';

@NgModule({
  declarations: [
    ComfirmPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(ComfirmPlanPage),
  ],
})
export class ComfirmPlanPageModule {}
