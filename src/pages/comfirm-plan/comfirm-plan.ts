import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreatePlanPage } from '../create-plan/create-plan';

@IonicPage()
@Component({
  selector: 'page-comfirm-plan',
  templateUrl: 'comfirm-plan.html',
})
export class ComfirmPlanPage {
  showModal: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComfirmPlanPage');
  }
  pubFankui(){
    this.showModal = true;
  }
  createPlan(){
    this.navCtrl.push(CreatePlanPage);
  }
} 
