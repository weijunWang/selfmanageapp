import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { CaseNoticePage } from '../case-notice/case-notice';
import { HomeNoticePage } from '../home-notice/home-notice';
import { ChatGroupPage } from '../chat-group/chat-group';
import { RecordBloodPressurePage } from '../record-blood-pressure/record-blood-pressure';
import { QuestionnairePage } from '../questionnaire/questionnaire';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  listData: any;
  canShow: boolean = false;
  pushModal: boolean = false;
  modalTextArr: any;
  constructor(public navCtrl: NavController) {

  }
  ngOnInit(){
    this.canShow = true;
    this.listData = [
      {
        title: '公告',
        img: '../../assets/imgs/news.png'
      },{
        title: '病种公告',
        img: '../../assets/imgs/news2.png'
      },{
        title: '小组',
        img: '../../assets/imgs/group.png'
      }
    ]
    this.pushModal = true;
    this.modalTextArr = [
      {
        title: '计划提醒',
        text: '您要开始做本周的计划了，请尽快做出你自己的计划哦！为了更好的自我管理，请尽快进行哦！'
      },{
        title: '计划提醒',
        text: '您今天的计划完成了嘛？快去完成今天的计划吧，今天的计划是饭后10分钟散步！'
      },{
        title: '问卷调查',
        text: '为了更好的进行自我管理，我们计划为您提供个性化的管理，请填写相关问卷'
      }
    ]
  }
  toChat(){
    this.navCtrl.push(ChatPage);
  }
  toClass(){
    this.navCtrl.parent.select(1);
  }
  toNotice(index){
    if(index == 0){
      this.navCtrl.push(HomeNoticePage);
    }else if(index == 1){
      this.navCtrl.push(CaseNoticePage);
    }else if(index == 2){
      this.navCtrl.push(ChatGroupPage);
    }
  }
  toRecordBlood(){
    this.navCtrl.push(RecordBloodPressurePage);
  }
  checkConform(){
    this.navCtrl.push(QuestionnairePage);
  }
  closeModal(){
    this.pushModal = false;
  }
}
