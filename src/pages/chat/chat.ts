import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Content, NavParams } from 'ionic-angular';
import { ChatInfoPage } from '../chat-info/chat-info';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  toUser = {
    _id: '534b8e5aaa5e7afc1b23e69b',
    pic: '../../assets/imgs/me.png',
    username: 'Venkman',
  };

  user = {
    _id: '534b8fb2aa5e7afc1b23e69c',
    pic: '../../assets/imgs/me.png',
    username: 'Marty',
  };
  showNewsNum: boolean = true;
  doneLoading = false;

  messages = [
    {
      _id: 1,
      date: new Date(),
      userId: this.user._id,
      username: this.user.username,
      pic: this.user.pic,
      text: 'OH CRAP!!'
    },
    {
      _id: 2,
      date: new Date(),
      userId: this.toUser._id,
      username: this.toUser.username,
      pic: this.toUser.pic,
      text: 'what??'
    },
    {
      _id: 3,
      date: new Date(),
      userId: this.toUser._id,
      username: this.toUser.username,
      pic: this.toUser.pic,
      text: 'Pretty long message with lots of content'
    },
    {
      _id: 4,
      date: new Date(),
      userId: this.user._id,
      username: this.user.username,
      pic: this.user.pic,
      text: 'Pretty long message with even way more of lots and lots of content'
    },
    {
      _id: 5,
      date: new Date(),
      userId: this.user._id,
      username: this.user.username,
      pic: this.user.pic,
      text: '哪尼??'
    },
    {
      _id: 6,
      date: new Date(),
      userId: this.toUser._id,
      username: this.toUser.username,
      pic: this.toUser.pic,
      text: 'yes!'
    }
  ];
  @ViewChild(Content) content: Content;
  audio: boolean = false;
  chatBox: any;
  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
  ) {
  }

  ionViewDidLoad() {
    let modelData: string = '用户名：' + this.navParams.get('chatId');
    console.log(modelData);
  }
  // 发送消息
  send(message) {
    if (message && message !== '') {
      // this.messageService.sendMessage(chatId, message);

      const messageData =
      {
        toId: this.toUser._id,
        _id: 6,
        date: new Date(),
        userId: this.user._id,
        username: this.toUser.username,
        pic: this.toUser.pic,
        text: message
      };

      this.messages.push(messageData);
      this.scrollToBottom();

      setTimeout(() => {
        const replyData =
        {
          toId: this.toUser._id,
          _id: 6,
          date: new Date(),
          userId: this.toUser._id,
          username: this.toUser.username,
          pic: this.toUser.pic,
          text: 'Just a quick reply'
        };
        this.messages.push(replyData);
        this.scrollToBottom();
      }, 1000);
    }
    this.chatBox = '';
  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }

  viewProfile(message: string) {
    console.log(message);
  }
  doRefresh(refresher) {
    console.log("下拉刷新");
    setTimeout(() => {
      console.log('加载完成后，关闭刷新');
      refresher.complete();

      //toast提示
      // this.showInfo("加载成功");
    }, 2000);
  }
  changeAudio(){
    console.log(121);
    if(this.audio){
      this.audio = false
    }else{
      this.audio = true;
    }
  }
  btnTouchstart(){
    console.log('start');
    
  }
  btnTouchend(){
    console.log('end');
  }
  allNews(){
    this.showNewsNum = false;
  }
  chatGroup(){
    this.navCtrl.push(ChatInfoPage);
  }
}
