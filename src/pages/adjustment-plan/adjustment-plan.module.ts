import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdjustmentPlanPage } from './adjustment-plan';

@NgModule({
  declarations: [
    AdjustmentPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(AdjustmentPlanPage),
  ],
})
export class AdjustmentPlanPageModule {}
