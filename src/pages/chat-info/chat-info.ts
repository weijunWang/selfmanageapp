import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ChatGroupPage } from '../chat-group/chat-group';
import { ChatRecordPage } from '../chat-record/chat-record';

@IonicPage()
@Component({
  selector: 'page-chat-info',
  templateUrl: 'chat-info.html',
})
export class ChatInfoPage {
  isBegin: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatInfoPage');
  }
  choiceBegin(){
    this.isBegin = true;
  }
  choiceClose(){
    this.isBegin = false;
  }
  choiceQuit(){

  }
  toGroup(){
    this.navCtrl.push(ChatGroupPage);
  }
  toRecord(){
    this.navCtrl.push(ChatRecordPage);
  }
}
