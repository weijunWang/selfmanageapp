import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BloodPressureDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blood-pressure-data',
  templateUrl: 'blood-pressure-data.html',
})
export class BloodPressureDataPage {
  pushModal: boolean = false;
  modalTextArr: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BloodPressureDataPage');
    
  }
  checkBloodPressure(){

  }
  checkConform(){

  }
  closeModal(){
    this.pushModal = false;
  }
}
