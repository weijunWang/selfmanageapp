import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BloodPressureDataPage } from './blood-pressure-data';

@NgModule({
  declarations: [
    BloodPressureDataPage,
  ],
  imports: [
    IonicPageModule.forChild(BloodPressureDataPage),
  ],
})
export class BloodPressureDataPageModule {}
