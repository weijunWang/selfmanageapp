import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-awaiting-audit',
  templateUrl: 'awaiting-audit.html',
})
export class AwaitingAuditPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AwaitingAuditPage');
  }
  choiceGood(){
    this.navCtrl.push(TabsPage);
  }
}
