import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AwaitingAuditPage } from './awaiting-audit';

@NgModule({
  declarations: [
    AwaitingAuditPage,
  ],
  imports: [
    IonicPageModule.forChild(AwaitingAuditPage),
  ],
})
export class AwaitingAuditPageModule {}
