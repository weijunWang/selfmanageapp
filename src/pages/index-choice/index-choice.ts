import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InpuUserinfoPage } from '../inpu-userinfo/inpu-userinfo';

@IonicPage()
@Component({
  selector: 'page-index-choice',
  templateUrl: 'index-choice.html',
})
export class IndexChoicePage {
  listData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndexChoicePage');
    this.listData = [
      {
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      },{
        flag: false,
        text: '病例一种'
      }
    ]
  }
  showItem(index){
    for(let i = 0;i < this.listData.length;i++){  
      this.listData[i].flag = false
    }
    this.listData[index].flag = true;
  }
  choiceGood(){
    for(let i = 0;i < this.listData.length;i++){  
      if(this.listData[i].flag){
        console.log(this.listData[i].text);
      }
    }
    this.navCtrl.push(InpuUserinfoPage);
  }
}
