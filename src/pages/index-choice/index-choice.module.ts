import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndexChoicePage } from './index-choice';

@NgModule({
  declarations: [
    IndexChoicePage,
  ],
  imports: [
    IonicPageModule.forChild(IndexChoicePage),
  ],
})
export class IndexChoicePageModule {}
