import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchPage } from '../search/search';
import { ChoiceItemPage } from '../choice-item/choice-item';

@IonicPage()
@Component({
  selector: 'page-choice-position',
  templateUrl: 'choice-position.html',
})
export class ChoicePositionPage {
  areas: any;
  streets: any;
  juwei: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.areas = [
      {
        name: 'col1',
        options: [
          { text: '1', value: '1' },
          { text: '2', value: '2' },
          { text: '3', value: '3' }
        ]
      }
    ]
    this.streets = [
      {
        name: 'col1',
        options: [
          { text: '1', value: '1' },
          { text: '2', value: '2' },
          { text: '3', value: '3' }
        ]
      }
    ]
    this.juwei = [
      {
        name: 'col1',
        options: [
          { text: '1', value: '1' },
          { text: '2', value: '2' },
          { text: '3', value: '3' }
        ]
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoicePositionPage');
  }
  search(){
    this.navCtrl.push(SearchPage);
  }
  choiceGood(){
    this.navCtrl.push(ChoiceItemPage);
  }
  areaCli(){

  }
  streetCli(){

  }
  neighborhoodCommittee(){

  }
}
