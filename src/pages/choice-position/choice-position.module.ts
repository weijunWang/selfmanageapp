import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoicePositionPage } from './choice-position';

@NgModule({
  declarations: [
    ChoicePositionPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoicePositionPage),
  ],
})
export class ChoicePositionPageModule {}
