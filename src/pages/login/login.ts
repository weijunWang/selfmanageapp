import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { IndexChoicePage } from '../index-choice/index-choice';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  isHide: boolean = false;
  showPhone: boolean = true;
  codeString: string = '获取验证码';
  codeNum: any = "";
  timer: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit(){
    console.log('LoginPage');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  toRegister(){
    console.log("toRegister");
  }
  changeLoginType(){
    if(this.showPhone){
      this.showPhone = false;
    }else{
      this.showPhone = true;
    }
  }
  getCode() {
    clearInterval(this.timer);
    console.log("获取验证码");
    this.codeString = "重新获取";
    this.codeNum = 60;
    this.timer = setInterval(() => {
      if (this.codeNum <= 0) {
        this.codeNum = 0;
        clearInterval(this.timer);
      } else {
        this.codeNum--;
      }
      console.log(this.codeNum);
    }, 1000);
  }
  toForgetPwd(){
    console.log("toForgetPwd");
    // this.navCtrl.push(ForgetPwdPage);
  }
  login(){
    this.navCtrl.push(IndexChoicePage)
  }
  focusInput(){
    this.isHide = true;
  }
  blurInput(){
    this.isHide = false;
  }
}
