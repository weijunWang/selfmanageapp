import { Component, ViewChild } from '@angular/core';
import { Tabs } from 'ionic-angular';

import { PlanPage } from '../plan/plan';
import { ClassPage } from '../class/class';
import { HomePage } from '../home/home';
import { MinePage } from '../mine/mine';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild('myTabs') tabRef: Tabs;
  canShow: boolean = true;
  tab1Root = HomePage;
  tab2Root = ClassPage;
  tab3Root = PlanPage;
  tab4Root = MinePage;

  constructor() {
  }
  ionViewDidEnter(){
    // this.tabRef.select(2);
  }
  ngOnInit(){
    
    // this.canShow = true;
  }
}
