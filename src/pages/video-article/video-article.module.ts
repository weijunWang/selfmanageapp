import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoArticlePage } from './video-article';

@NgModule({
  declarations: [
    VideoArticlePage,
  ],
  imports: [
    IonicPageModule.forChild(VideoArticlePage),
  ],
})
export class VideoArticlePageModule {}
