import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeNoticePage } from './home-notice';

@NgModule({
  declarations: [
    HomeNoticePage,
  ],
  imports: [
    IonicPageModule.forChild(HomeNoticePage),
  ],
})
export class HomeNoticePageModule {}
