import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NoticeDetailPage } from '../notice-detail/notice-detail';
import { CreateNoticePage } from '../create-notice/create-notice';

@IonicPage()
@Component({
  selector: 'page-home-notice',
  templateUrl: 'home-notice.html',
})
export class HomeNoticePage {
  listData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatGroupPage');
    this.listData = [
      {
        notice: '石门二路街道善昌居委会社区活动；',
        address: '静安区石门二路街道善昌居委会',
        time: '2月13日 14：30',
        content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
        looks: 10,
        userTitle: '组长张清晨',
        dateTime: '2019年1月20日'
      },{
        notice: '石门二路街道善昌居委会社区活动；',
        address: '静安区石门二路街道善昌居委会',
        time: '2月13日 14：30',
        content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
        looks: 10,
        userTitle: '组长张清晨',
        dateTime: '2019年1月20日'
      },{
        notice: '石门二路街道善昌居委会社区活动；',
        address: '静安区石门二路街道善昌居委会',
        time: '2月13日 14：30',
        content: '的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者的老年生活由自己管理，我们将在社区举行糖尿病患者社区举行糖尿病患者',
        looks: 10,
        userTitle: '组长张清晨',
        dateTime: '2019年1月20日'
      }
    ]
  }
  createNotice(){
    this.navCtrl.push(CreateNoticePage);
  }
  toNoticeDetail(){
    this.navCtrl.push(NoticeDetailPage);
  }
}
