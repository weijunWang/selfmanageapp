import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordBloodPressurePage } from './record-blood-pressure';

@NgModule({
  declarations: [
    RecordBloodPressurePage,
  ],
  imports: [
    IonicPageModule.forChild(RecordBloodPressurePage),
  ],
})
export class RecordBloodPressurePageModule {}
