import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BloodPressureDataPage } from '../blood-pressure-data/blood-pressure-data';

@IonicPage()
@Component({
  selector: 'page-record-blood-pressure',
  templateUrl: 'record-blood-pressure.html',
})
export class RecordBloodPressurePage {
  juwei: any;
  myDate: any;
  hourMinute: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.juwei = [
      {
        name: 'col1',
        options: [
          { text: '1', value: '1' },
          { text: '2', value: '2' },
          { text: '3', value: '3' }
        ]
      }
    ]
    this.hourMinute = [
      {
        name: 'hours',
        options: [
          { text: '8：00——10：00', value: '0' },
          { text: '16：00——18：00', value: '1' }
        ]
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecordBloodPressurePage');
    
  }
  publicNotice(){
    this.navCtrl.push(BloodPressureDataPage);
  }
}
