import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChatRecordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-record',
  templateUrl: 'chat-record.html',
})
export class ChatRecordPage {
  storgeData: any;
  allData: any;
  keyword: string;
  searching: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatRecordPage');
  }
  ngOnInit() {
    this.storgeData = [
        {
        id: 1,
        text: "孕早期禁食有哪些？",
      },{
        id: 2,
        text: "孕期运动",
      },{
        id: 3,
        text: "孕早期",
      },{
        id: 4,
        text: "妊娠期",
      }
    ]
    this.allData = [
      {
        id: 1,
        content: '石门二路街道善昌居委会',
      },{
        id: 2,
        content: '消息在群公告内，你可以去查看消息在群公告内，你可以去查看',
      },{
        id: 3,
        content: '石门二路街道善昌居委会(本部)',
      }
    ]
    console.log('ionViewDidLoad KnowledgeBasePage');
  }
  toPage(id){
    // this.navCtrl.push(KnowledgeDetailPage);
  }
  inputFocus(){
    if(this.keyword != ''){
      this.searching = true;
    }
  }
  clearAll(){
    this.storgeData = [];
  }
}
