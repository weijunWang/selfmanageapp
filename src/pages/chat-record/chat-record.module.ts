import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatRecordPage } from './chat-record';

@NgModule({
  declarations: [
    ChatRecordPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatRecordPage),
  ],
})
export class ChatRecordPageModule {}
