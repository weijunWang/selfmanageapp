import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AwaitingAuditPage } from '../awaiting-audit/awaiting-audit';
/**
 * Generated class for the ChoiceItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-choice-item',
  templateUrl: 'choice-item.html',
})
export class ChoiceItemPage {
  listData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoiceItemPage');
    this.listData = [
      {
        show: false,
        title1: '徐汇石门二路街道善昌居委—糖尿病A组',
        title2: '每周三14：30'
      },{
        show: false,
        title1: '徐汇石门二路街道善昌居委—糖尿病A组',
        title2: '每周三14：30'
      },{
        show: false,
        title1: '徐汇石门二路街道善昌居委—糖尿病A组',
        title2: '每周三14：30'
      }
    ]
  }
  search(){
    // this.navCtrl.push(SearchPage);
  }
  choiceCli(index){
    for(let i = 0;i < this.listData.length;i++){
      this.listData[i].show = false;
    }
    this.listData[index].show = true;
  }
  neighborhoodCommittee(){

  }
  choiceGood(){
    this.navCtrl.push(AwaitingAuditPage);
  }
}
