import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoiceItemPage } from './choice-item';

@NgModule({
  declarations: [
    ChoiceItemPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoiceItemPage),
  ],
})
export class ChoiceItemPageModule {}
