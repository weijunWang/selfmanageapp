import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ComfirmPlanPage } from '../comfirm-plan/comfirm-plan';
import { AdjustmentPlanPage } from '../adjustment-plan/adjustment-plan';

@Component({
  selector: 'page-plan',
  templateUrl: 'plan.html'
})
export class PlanPage {
  @Input("slides") slides: string[] = [];
  @Input("pageNumber") pageNumber: number = 2;
  @Output("slideClick") slideClick = new EventEmitter<number>();

  mySlideOptions;
  selectedIndex: number = 0;
  weekData: any;
  val: boolean = false;
  evaData = [false, false, false];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.weekData = [
      {
        flag: true,
        text: '日'
      },{
        flag: false,
        text: '一'
      },{
        flag: false,
        text: '二'
      },{
        flag: false,
        text: '三'
      },{
        flag: false,
        text: '四'
      },{
        flag: false,
        text: '五'
      },{
        flag: false,
        text: '六'
      }
    ]
    this.mySlideOptions = {
      loop: false,
      autoplay: false,
      initialSlide: 0,
      pager: false,
      slidesPerView: this.pageNumber,
      paginationHide: true,
      paginationClickable: true
    }
    this.slides = ['计划中', '已完成']
  }
  onClick(index) {
    this.selectedIndex = index;
    this.slideClick.emit(index);
  }
  outpust(data){
    console.log(data);
  }
  evaOut(data){
    console.log(data);
    this.val = true;
  }
  confirmPlan(){
    this.navCtrl.push(ComfirmPlanPage);
  }
  editItem(item){
    this.navCtrl.push(AdjustmentPlanPage);
  }
}
