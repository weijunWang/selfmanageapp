import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChatGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-group',
  templateUrl: 'chat-group.html',
})
export class ChatGroupPage {
  listData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatGroupPage');
    this.listData = [
      {
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      },{
        img: '../../assets/imgs/man.png',
        name: '王爱华',
        title: '组长'
      }
    ]
  }

}
