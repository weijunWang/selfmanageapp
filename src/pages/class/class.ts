import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VideoArticlePage } from '../video-article/video-article';

@Component({
  selector: 'page-class',
  templateUrl: 'class.html'
})
export class ClassPage {
  @Input("slides") slides: string[] = [];
  @Input("pageNumber") pageNumber: number = 5;
  @Output("slideClick") slideClick = new EventEmitter<number>();
 
  mySlideOptions;
  selectedIndex: number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.mySlideOptions = {
      loop: false,
      autoplay: false,
      initialSlide: 0,
      pager: false,
      slidesPerView: this.pageNumber,
      paginationHide: true,
      paginationClickable: true
    }
    this.slides = ['文章', '音频', '视频']
  }
  onClick(index) {
    this.selectedIndex = index;
    this.slideClick.emit(index);
  }
  toVideo(){
    this.navCtrl.push(VideoArticlePage);
  }
}
