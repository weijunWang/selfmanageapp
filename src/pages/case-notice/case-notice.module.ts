import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaseNoticePage } from './case-notice';

@NgModule({
  declarations: [
    CaseNoticePage,
  ],
  imports: [
    IonicPageModule.forChild(CaseNoticePage),
  ],
})
export class CaseNoticePageModule {}
