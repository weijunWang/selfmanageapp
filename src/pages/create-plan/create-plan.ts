import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreatePlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-plan',
  templateUrl: 'create-plan.html',
})
export class CreatePlanPage {
  juwei: any;
  myDate: any;
  hourMinute: any;
  showPlan: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.juwei = [
      {
        name: 'col1',
        options: [
          { text: '1', value: '1' },
          { text: '2', value: '2' },
          { text: '3', value: '3' }
        ]
      }
    ]
    this.hourMinute = [
      {
        name: 'hours',
        options: [
          { text: '8：00——10：00', value: '0' },
          { text: '16：00——18：00', value: '1' }
        ]
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecordBloodPressurePage');
  }
  publicPlan(){
    this.showPlan = true;
  }
}
