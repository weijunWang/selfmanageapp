import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnairePage } from './questionnaire';
import { SignaturePadModule } from 'angular2-signaturepad';



@NgModule({
  declarations: [
    QuestionnairePage,
  ],
  imports: [
    SignaturePadModule,
    IonicPageModule.forChild(QuestionnairePage),
  ],
})
export class QuestionnairePageModule {}
