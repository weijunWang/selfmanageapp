import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';


/**
 * Generated class for the QuestionnairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html',
})
export class QuestionnairePage {
  @ViewChild(SignaturePad) public signaturePad: SignaturePad; //第二视图
  signatureImage: string; //定义类型
  signaturePadOptions: Object = {
    'minWidth': 2,
    'canvasWidth': 220,
    'canvasHeight': 120
  };
  showModal: boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnairePage');
  }
  ngAfterViewInit() {
    this.signaturePad.clear();
    this.canvasResize();
  }
  // 清除模板

  drawClear() {
    this.signaturePad.clear();
  }
  canvasResize() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 2);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
  }
  
  // 完成生成图片
  drawComplete(sign) {
    this.signatureImage = this.signaturePad.toDataURL();
    console.log(this.signatureImage);
  }
  knows(){
    this.showModal = false;
  }
}
