import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InpuUserinfoPage } from './inpu-userinfo';

@NgModule({
  declarations: [
    InpuUserinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(InpuUserinfoPage),
  ],
})
export class InpuUserinfoPageModule {}
