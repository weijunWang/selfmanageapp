import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ChoicePositionPage } from '../choice-position/choice-position';

@IonicPage()
@Component({
  selector: 'page-inpu-userinfo',
  templateUrl: 'inpu-userinfo.html',
})
export class InpuUserinfoPage {
  showRight: Number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InpuUserinfoPage');
  }
  choiceGood(){
    this.navCtrl.push(ChoicePositionPage);
  }
  choiceMan(){
    this.showRight = 1;
  }
  choiceWoman(){
    this.showRight = 2;
  }
}
