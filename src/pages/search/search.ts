import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  storgeData: any;
  allData: any;
  keyword: string;
  searching: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  ngOnInit() {
    this.storgeData = [
        {
        id: 1,
        text: "孕早期禁食有哪些？",
      },{
        id: 2,
        text: "孕期运动",
      },{
        id: 3,
        text: "孕早期",
      },{
        id: 4,
        text: "妊娠期",
      }
    ]
    this.allData = [
      {
        id: 1,
        content: '石门二路街道善昌居委会',
      },{
        id: 2,
        content: '石门二路街道善昌居委会',
      },{
        id: 3,
        content: '石门二路街道善昌居委会(本部)',
      }
    ]
    console.log('ionViewDidLoad KnowledgeBasePage');
  }
  toPage(id){
    // this.navCtrl.push(KnowledgeDetailPage);
  }
  inputFocus(){
    if(this.keyword != ''){
      this.searching = true;
    }
  }
  clearAll(){
    this.storgeData = [];
  }
}
