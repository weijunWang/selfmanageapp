import { NavController } from 'ionic-angular';
import { Component, Input,Output,EventEmitter } from '@angular/core';

/**
 * Generated class for the CheckboxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'checkbox',
  templateUrl: 'checkbox.html'
})
export class CheckboxComponent {
  isChecked: boolean = false;
  text: string;
  @Input() values:any; //获取从父组件传递过来的数据
  @Output() out:EventEmitter<any> = new EventEmitter;
  constructor(public navCtrl:NavController) {
    console.log('Hello CheckboxComponent Component');
    this.text = 'Hello World';
  }
  ngOnInit(){
    console.log(this.values);
    this.values = false;
  }
  checkCli(){
    if(this.values){
      return false;
    }
    if(this.isChecked){
      this.isChecked = false;
    }else{
      this.isChecked = true;
    }
    this.out.emit(this.isChecked);
  }
}
