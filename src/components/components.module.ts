import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckboxComponent } from './checkbox/checkbox';
import { EvaluateComponent } from './evaluate/evaluate';
@NgModule({
	declarations: [CheckboxComponent,
    EvaluateComponent],
	imports: [CommonModule],
	exports: [CheckboxComponent,
    EvaluateComponent]
})
export class ComponentsModule {}
