import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the EvaluateComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'evaluate',
  templateUrl: 'evaluate.html'
})
export class EvaluateComponent {
  @Input() datas: any;
  @Output() out: EventEmitter<any> = new EventEmitter;
  listData: any;
  constructor() {
    console.log('Hello EvaluateComponent Component');
    this.listData = [
      {
        isChecked: false,
        img: '../../assets/imgs/lian1.png',
        imgChecked: '../../assets/imgs/lian1-checked.png'
      },{
        isChecked: false,
        img: '../../assets/imgs/lian2.png',
        imgChecked: '../../assets/imgs/lian2-checked.png'
      },{
        isChecked: false,
        img: '../../assets/imgs/lian3.png',
        imgChecked: '../../assets/imgs/lian3-checked.png'
      }
    ]
  }
  ngOnInit(){
    console.log(this.datas);
  }
  checkedCli(index){
    for(let i = 0;i < this.listData.length;i++){
      this.listData[i].isChecked = false;
    }
    this.listData[index].isChecked = true;
    this.out.emit(this.listData);
  }
}
